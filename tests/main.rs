use bfbfs_rust::shared;
use std::collections::VecDeque;

fn setup_graph_a() -> VecDeque<Vec<usize>> {
    return VecDeque::from_iter([
        vec![0, 3, 4, 0, 0, 0],
        vec![3, 0, 3, 5, 2, 0],
        vec![4, 3, 0, 0, 6, 0],
        vec![0, 5, 0, 0, 1, 3],
        vec![0, 2, 6, 1, 0, 1],
        vec![0, 0, 0, 3, 1, 0]
    ]);
}

fn setup_graph_b() -> VecDeque<Vec<usize>> {
    return VecDeque::from_iter([
        vec![0, 450, 900, 800, 0, 0, 0, 0],
        vec![450, 0, 0, 0, 1800, 900, 0, 0],
        vec![950, 0, 0, 0, 1100, 600, 0, 0],
        vec![800, 0, 0, 0, 0, 600, 1200, 0],
        vec![0, 1800, 1100, 0, 0, 900, 0, 400],
        vec![0, 900, 600, 600, 900, 0, 1000, 1400],
        vec![0, 0, 0, 1200, 0, 1000, 0, 600],
        vec![0, 0, 0, 0, 400, 1300, 600, 0]
    ]);
}

fn setup_graph_c() -> VecDeque<Vec<usize>> {
    return VecDeque::from_iter([
        vec![0, 8, 8, 1, 3],
        vec![8, 0, 6, 3, 2],
        vec![8, 6, 0, 5, 2],
        vec![1, 3, 5, 0, 2],
        vec![3, 2, 2, 1, 0]
    ]);
}

fn setup_graph_d() -> VecDeque<Vec<usize>> {
    return VecDeque::from_iter([
        vec![0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0],
        vec![1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0],
        vec![1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        vec![1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0],
        vec![0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0],
        vec![0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        vec![0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1],
        vec![0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
        vec![0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
        vec![0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
        vec![0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
        vec![0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
    ]);
}

#[test]
fn test_dummy() {
    assert_eq!("2", shared::dummy_method("2"))
}

#[test]
fn test_traverse_a16() {
    let graph: VecDeque<Vec<usize>> = setup_graph_a();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![0, 1, 3, 5],
        vec![0, 1, 4, 5],
        vec![0, 2, 4, 5],
        vec![0, 1, 2, 4, 5],
        vec![0, 1, 3, 4, 5],
        vec![0, 2, 1, 4, 5],
        vec![0, 1, 4, 3, 5],
        vec![0, 2, 4, 3, 5],
        vec![0, 1, 2, 4, 3, 5],
        vec![0, 2, 1, 3, 5],
        vec![0, 2, 4, 1, 3, 5],
        vec![0, 2, 1, 4, 3, 5],
        vec![0, 2, 1, 3, 4, 5]
    ]);

    assert_eq!(result, shared::traverse(&graph, 0, 5));
}

#[test]
fn test_traverse_a15() {
    let graph: VecDeque<Vec<usize>> = setup_graph_a();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![0, 1, 4],
        vec![0, 2, 4],
        vec![0, 1, 2, 4],
        vec![0, 1, 3, 4],
        vec![0, 2, 1, 4],
        vec![0, 1, 3, 5, 4],
        vec![0, 2, 1, 3, 4],
        vec![0, 2, 1, 3, 5, 4]
    ]);

    assert_eq!(result, shared::traverse(&graph, 0, 4));
}

#[test]
fn test_traverse_a14() {
    let graph: VecDeque<Vec<usize>> = setup_graph_a();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![0, 1, 3],
        vec![0, 1, 4, 3],
        vec![0, 2, 4, 3],
        vec![0, 1, 2, 4, 3],
        vec![0, 2, 1, 3],
        vec![0, 2, 4, 1, 3],
        vec![0, 2, 1, 4, 3],
        vec![0, 1, 4, 5, 3],
        vec![0, 2, 4, 5, 3],
        vec![0, 1, 2, 4, 5, 3],
        vec![0, 2, 1, 4, 5, 3]
    ]);

    assert_eq!(result, shared::traverse(&graph, 0, 3));
}

#[test]
fn test_traverse_a13() {
    let graph: VecDeque<Vec<usize>> = setup_graph_a();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![0, 2],
        vec![0, 1, 2],
        vec![0, 1, 4, 2],
        vec![0, 1, 3, 4, 2],
        vec![0, 1, 3, 5, 4, 2]
    ]);

    assert_eq!(result, shared::traverse(&graph, 0, 2));
}

#[test]
fn test_traverse_a12() {
    let graph: VecDeque<Vec<usize>> = setup_graph_a();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![0, 1],
        vec![0, 2, 1],
        vec![0, 2, 4, 1],
        vec![0, 2, 4, 3, 1],
        vec![0, 2, 4, 5, 3, 1]
    ]);

    assert_eq!(result, shared::traverse(&graph, 0, 1));
}

#[test]
fn test_traverse_a26() {
    let graph: VecDeque<Vec<usize>> = setup_graph_a();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![1, 3, 5],
        vec![1, 4, 5],
        vec![1, 2, 4, 5],
        vec![1, 0, 2, 4, 5],
        vec![1, 3, 4, 5],
        vec![1, 4, 3, 5],
        vec![1, 2, 4, 3, 5],
        vec![1, 0, 2, 4, 3, 5]
    ]);

    assert_eq!(result, shared::traverse(&graph, 1, 5));
}

#[test]
fn test_traverse_a25() {
    let graph: VecDeque<Vec<usize>> = setup_graph_a();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![1, 4],
        vec![1, 2, 4],
        vec![1, 0, 2, 4],
        vec![1, 3, 4],
        vec![1, 3, 5, 4]
    ]);

    assert_eq!(result, shared::traverse(&graph, 1, 4));
}

#[test]
fn test_traverse_a24() {
    let graph: VecDeque<Vec<usize>> = setup_graph_a();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![1, 3],
        vec![1, 4, 3],
        vec![1, 2, 4, 3],
        vec![1, 0, 2, 4, 3],
        vec![1, 4, 5, 3],
        vec![1, 2, 4, 5, 3],
        vec![1, 0, 2, 4, 5, 3]
    ]);

    assert_eq!(result, shared::traverse(&graph, 1, 3));
}

#[test]
fn test_traverse_a23() {
    let graph: VecDeque<Vec<usize>> = setup_graph_a();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![1, 2],
        vec![1, 0, 2],
        vec![1, 4, 2],
        vec![1, 3, 4, 2],
        vec![1, 3, 5, 4, 2]
    ]);

    assert_eq!(result, shared::traverse(&graph, 1, 2));
}

#[test]
fn test_traverse_a21() {
    let graph: VecDeque<Vec<usize>> = setup_graph_a();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![1, 0],
        vec![1, 2, 0],
        vec![1, 4, 2, 0],
        vec![1, 3, 4, 2, 0],
        vec![1, 3, 5, 4, 2, 0]
    ]);

    assert_eq!(result, shared::traverse(&graph, 1, 0));
}

#[test]
fn test_traverse_a36() {
    let graph: VecDeque<Vec<usize>> = setup_graph_a();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![2, 4, 5],
        vec![2, 1, 4, 5],
        vec![2, 0, 1, 4, 5],
        vec![2, 1, 3, 5],
        vec![2, 0, 1, 3, 5],
        vec![2, 4, 3, 5],
        vec![2, 1, 4, 3, 5],
        vec![2, 0, 1, 4, 3, 5],
        vec![2, 4, 1, 3, 5],
        vec![2, 1, 3, 4, 5],
        vec![2, 0, 1, 3, 4, 5]
    ]);

    assert_eq!(result, shared::traverse(&graph, 2, 5));
}

#[test]
fn test_traverse_a35() {
    let graph: VecDeque<Vec<usize>> = setup_graph_a();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![2, 4],
        vec![2, 1, 4],
        vec![2, 0, 1, 4],
        vec![2, 1, 3, 4],
        vec![2, 0, 1, 3, 4],
        vec![2, 1, 3, 5, 4],
        vec![2, 0, 1, 3, 5, 4]
    ]);

    assert_eq!(result, shared::traverse(&graph, 2, 4));
}

#[test]
fn test_traverse_a34() {
    let graph: VecDeque<Vec<usize>> = setup_graph_a();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![2, 1, 3],
        vec![2, 0, 1, 3],
        vec![2, 4, 3],
        vec![2, 1, 4, 3],
        vec![2, 0, 1, 4, 3],
        vec![2, 4, 1, 3],
        vec![2, 4, 5, 3],
        vec![2, 1, 4, 5, 3],
        vec![2, 0, 1, 4, 5, 3]
    ]);

    assert_eq!(result, shared::traverse(&graph, 2, 3));
}

#[test]
fn test_traverse_a32() {
    let graph: VecDeque<Vec<usize>> = setup_graph_a();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![2, 1],
        vec![2, 0, 1],
        vec![2, 4, 1],
        vec![2, 4, 3, 1],
        vec![2, 4, 5, 3, 1]
    ]);

    assert_eq!(result, shared::traverse(&graph, 2, 1));
}

#[test]
fn test_traverse_a31() {
    let graph: VecDeque<Vec<usize>> = setup_graph_a();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![2, 0],
        vec![2, 1, 0],
        vec![2, 4, 1, 0],
        vec![2, 4, 3, 1, 0],
        vec![2, 4, 5, 3, 1, 0]
    ]);

    assert_eq!(result, shared::traverse(&graph, 2, 0));
}

#[test]
fn test_traverse_a46() {
    let graph: VecDeque<Vec<usize>> = setup_graph_a();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![3, 5],
        vec![3, 4, 5],
        vec![3, 1, 4, 5],
        vec![3, 1, 2, 4, 5],
        vec![3, 1, 0, 2, 4, 5]
    ]);

    assert_eq!(result, shared::traverse(&graph, 3, 5));
}

#[test]
fn test_traverse_a45() {
    let graph: VecDeque<Vec<usize>> = setup_graph_a();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![3, 4],
        vec![3, 1, 4],
        vec![3, 5, 4],
        vec![3, 1, 2, 4],
        vec![3, 1, 0, 2, 4]
    ]);

    assert_eq!(result, shared::traverse(&graph, 3, 4));
}

#[test]
fn test_traverse_a43() {
    let graph: VecDeque<Vec<usize>> = setup_graph_a();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![3, 1, 2],
        vec![3, 4, 2],
        vec![3, 1, 4, 2],
        vec![3, 1, 0, 2],
        vec![3, 5, 4, 2],
        vec![3, 4, 1, 2],
        vec![3, 5, 4, 1, 2],
        vec![3, 4, 1, 0, 2],
        vec![3, 5, 4, 1, 0, 2]
    ]);

    assert_eq!(result, shared::traverse(&graph, 3, 2));
}

#[test]
fn test_traverse_a42() {
    let graph: VecDeque<Vec<usize>> = setup_graph_a();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![3, 1],
        vec![3, 4, 1],
        vec![3, 4, 2, 1],
        vec![3, 5, 4, 1],
        vec![3, 5, 4, 2, 1],
        vec![3, 4, 2, 0, 1],
        vec![3, 5, 4, 2, 0, 1]
    ]);

    assert_eq!(result, shared::traverse(&graph, 3, 1));
}

#[test]
fn test_traverse_a41() {
    let graph: VecDeque<Vec<usize>> = setup_graph_a();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![3, 1, 0],
        vec![3, 1, 2, 0],
        vec![3, 4, 2, 0],
        vec![3, 1, 4, 2, 0],
        vec![3, 4, 1, 0],
        vec![3, 4, 2, 1, 0],
        vec![3, 5, 4, 1, 0],
        vec![3, 5, 4, 2, 0],
        vec![3, 4, 1, 2, 0],
        vec![3, 5, 4, 1, 2, 0],
        vec![3, 5, 4, 2, 1, 0]
    ]);

    assert_eq!(result, shared::traverse(&graph, 3, 0));
}

#[test]
fn test_traverse_a56() {
    let graph: VecDeque<Vec<usize>> = setup_graph_a();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![4, 5],
        vec![4, 3, 5],
        vec![4, 1, 3, 5],
        vec![4, 2, 1, 3, 5],
        vec![4, 2, 0, 1, 3, 5]
    ]);

    assert_eq!(result, shared::traverse(&graph, 4, 5));
}

#[test]
fn test_traverse_a54() {
    let graph: VecDeque<Vec<usize>> = setup_graph_a();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![4, 3],
        vec![4, 1, 3],
        vec![4, 5, 3],
        vec![4, 2, 1, 3],
        vec![4, 2, 0, 1, 3]
    ]);

    assert_eq!(result, shared::traverse(&graph, 4, 3));
}

#[test]
fn test_traverse_a53() {
    let graph: VecDeque<Vec<usize>> = setup_graph_a();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![4, 2],
        vec![4, 1, 2],
        vec![4, 1, 0, 2],
        vec![4, 3, 1, 2],
        vec![4, 5, 3, 1, 2],
        vec![4, 3, 1, 0, 2],
        vec![4, 5, 3, 1, 0, 2]
    ]);

    assert_eq!(result, shared::traverse(&graph, 4, 2));
}

#[test]
fn test_traverse_a52() {
    let graph: VecDeque<Vec<usize>> = setup_graph_a();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![4, 1],
        vec![4, 2, 1],
        vec![4, 3, 1],
        vec![4, 2, 0, 1],
        vec![4, 5, 3, 1]
    ]);

    assert_eq!(result, shared::traverse(&graph, 4, 1));
}

#[test]
fn test_traverse_a51() {
    let graph: VecDeque<Vec<usize>> = setup_graph_a();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![4, 1, 0],
        vec![4, 2, 0],
        vec![4, 1, 2, 0],
        vec![4, 2, 1, 0],
        vec![4, 3, 1, 0],
        vec![4, 5, 3, 1, 0],
        vec![4, 3, 1, 2, 0],
        vec![4, 5, 3, 1, 2, 0]
    ]);

    assert_eq!(result, shared::traverse(&graph, 4, 0));
}

#[test]
fn test_traverse_a65() {
    let graph: VecDeque<Vec<usize>> = setup_graph_a();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![5, 4],
        vec![5, 3, 4],
        vec![5, 3, 1, 4],
        vec![5, 3, 1, 2, 4],
        vec![5, 3, 1, 0, 2, 4]
    ]);

    assert_eq!(result, shared::traverse(&graph, 5, 4));
}

#[test]
fn test_traverse_a64() {
    let graph: VecDeque<Vec<usize>> = setup_graph_a();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![5, 3],
        vec![5, 4, 3],
        vec![5, 4, 1, 3],
        vec![5, 4, 2, 1, 3],
        vec![5, 4, 2, 0, 1, 3]
    ]);

    assert_eq!(result, shared::traverse(&graph, 5, 3));
}

#[test]
fn test_traverse_a63() {
    let graph: VecDeque<Vec<usize>> = setup_graph_a();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![5, 4, 2],
        vec![5, 3, 4, 2],
        vec![5, 3, 1, 2],
        vec![5, 4, 1, 2],
        vec![5, 3, 4, 1, 2],
        vec![5, 3, 1, 4, 2],
        vec![5, 3, 1, 0, 2],
        vec![5, 4, 1, 0, 2],
        vec![5, 3, 4, 1, 0, 2],
        vec![5, 4, 3, 1, 2],
        vec![5, 4, 3, 1, 0, 2]
    ]);

    assert_eq!(result, shared::traverse(&graph, 5, 2));
}

#[test]
fn test_traverse_a62() {
    let graph: VecDeque<Vec<usize>> = setup_graph_a();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![5, 3, 1],
        vec![5, 4, 1],
        vec![5, 3, 4, 1],
        vec![5, 4, 2, 1],
        vec![5, 3, 4, 2, 1],
        vec![5, 4, 3, 1],
        vec![5, 4, 2, 0, 1],
        vec![5, 3, 4, 2, 0, 1]
    ]);

    assert_eq!(result, shared::traverse(&graph, 5, 1));
}

#[test]
fn test_traverse_a61() {
    let graph: VecDeque<Vec<usize>> = setup_graph_a();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![5, 3, 1, 0],
        vec![5, 4, 1, 0],
        vec![5, 3, 4, 1, 0],
        vec![5, 4, 2, 0],
        vec![5, 3, 4, 2, 0],
        vec![5, 3, 1, 2, 0],
        vec![5, 4, 1, 2, 0],
        vec![5, 3, 4, 1, 2, 0],
        vec![5, 3, 1, 4, 2, 0],
        vec![5, 4, 2, 1, 0],
        vec![5, 3, 4, 2, 1, 0],
        vec![5, 4, 3, 1, 0],
        vec![5, 4, 3, 1, 2, 0]
    ]);

    assert_eq!(result, shared::traverse(&graph, 5, 0));
}

#[test]
fn test_traverse_b18() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![0, 1, 4, 7],
        vec![0, 2, 4, 7],
        vec![0, 1, 5, 7],
        vec![0, 2, 5, 7],
        vec![0, 3, 5, 7],
        vec![0, 1, 4, 5, 7],
        vec![0, 2, 4, 5, 7],
        vec![0, 1, 5, 4, 7],
        vec![0, 2, 5, 4, 7],
        vec![0, 3, 5, 4, 7],
        vec![0, 3, 6, 7],
        vec![0, 1, 5, 6, 7],
        vec![0, 2, 5, 6, 7],
        vec![0, 3, 5, 6, 7],
        vec![0, 1, 4, 5, 6, 7],
        vec![0, 2, 4, 5, 6, 7],
        vec![0, 3, 6, 5, 7],
        vec![0, 2, 4, 1, 5, 7],
        vec![0, 1, 4, 2, 5, 7],
        vec![0, 2, 5, 1, 4, 7],
        vec![0, 3, 5, 1, 4, 7],
        vec![0, 1, 5, 2, 4, 7],
        vec![0, 3, 5, 2, 4, 7],
        vec![0, 3, 6, 5, 4, 7],
        vec![0, 3, 6, 5, 1, 4, 7],
        vec![0, 3, 6, 5, 2, 4, 7],
        vec![0, 2, 4, 1, 5, 6, 7],
        vec![0, 1, 4, 2, 5, 6, 7],
        vec![0, 1, 5, 3, 6, 7],
        vec![0, 2, 5, 3, 6, 7],
        vec![0, 1, 4, 5, 3, 6, 7],
        vec![0, 2, 4, 5, 3, 6, 7],
        vec![0, 2, 4, 1, 5, 3, 6, 7],
        vec![0, 1, 4, 2, 5, 3, 6, 7]
    ]);

    assert_eq!(result, shared::traverse(&graph, 0, 7));
}

#[test]
fn test_traverse_b17() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![0, 3, 6],
        vec![0, 1, 5, 6],
        vec![0, 2, 5, 6],
        vec![0, 3, 5, 6],
        vec![0, 1, 4, 5, 6],
        vec![0, 2, 4, 5, 6],
        vec![0, 2, 4, 1, 5, 6],
        vec![0, 1, 4, 2, 5, 6],
        vec![0, 1, 4, 7, 6],
        vec![0, 2, 4, 7, 6],
        vec![0, 1, 5, 7, 6],
        vec![0, 2, 5, 7, 6],
        vec![0, 3, 5, 7, 6],
        vec![0, 1, 4, 5, 7, 6],
        vec![0, 2, 4, 5, 7, 6],
        vec![0, 1, 5, 4, 7, 6],
        vec![0, 2, 5, 4, 7, 6],
        vec![0, 3, 5, 4, 7, 6],
        vec![0, 2, 4, 1, 5, 7, 6],
        vec![0, 1, 4, 2, 5, 7, 6],
        vec![0, 1, 5, 3, 6],
        vec![0, 2, 5, 3, 6],
        vec![0, 1, 4, 5, 3, 6],
        vec![0, 2, 4, 5, 3, 6],
        vec![0, 2, 4, 1, 5, 3, 6],
        vec![0, 1, 4, 2, 5, 3, 6],
        vec![0, 2, 5, 1, 4, 7, 6],
        vec![0, 3, 5, 1, 4, 7, 6],
        vec![0, 1, 5, 2, 4, 7, 6],
        vec![0, 3, 5, 2, 4, 7, 6],
        vec![0, 1, 4, 7, 5, 6],
        vec![0, 2, 4, 7, 5, 6],
        vec![0, 1, 4, 7, 5, 3, 6],
        vec![0, 2, 4, 7, 5, 3, 6]
    ]);

    assert_eq!(result, shared::traverse(&graph, 0, 6));
}

#[test]
fn test_traverse_b16() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![0, 1, 5],
        vec![0, 2, 5],
        vec![0, 3, 5],
        vec![0, 1, 4, 5],
        vec![0, 2, 4, 5],
        vec![0, 3, 6, 5],
        vec![0, 2, 4, 1, 5],
        vec![0, 1, 4, 2, 5],
        vec![0, 1, 4, 7, 5],
        vec![0, 2, 4, 7, 5],
        vec![0, 3, 6, 7, 5],
        vec![0, 3, 6, 7, 4, 5],
        vec![0, 1, 4, 7, 6, 5],
        vec![0, 2, 4, 7, 6, 5],
        vec![0, 1, 4, 7, 6, 3, 5],
        vec![0, 2, 4, 7, 6, 3, 5],
        vec![0, 3, 6, 7, 4, 1, 5],
        vec![0, 3, 6, 7, 4, 2, 5]
    ]);

    assert_eq!(result, shared::traverse(&graph, 0, 5));
}

#[test]
fn test_traverse_b15() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![0, 1, 4],
        vec![0, 2, 4],
        vec![0, 1, 5, 4],
        vec![0, 2, 5, 4],
        vec![0, 3, 5, 4],
        vec![0, 2, 5, 1, 4],
        vec![0, 3, 5, 1, 4],
        vec![0, 1, 5, 2, 4],
        vec![0, 3, 5, 2, 4],
        vec![0, 3, 6, 5, 4],
        vec![0, 1, 5, 7, 4],
        vec![0, 2, 5, 7, 4],
        vec![0, 3, 5, 7, 4],
        vec![0, 3, 6, 7, 4],
        vec![0, 1, 5, 6, 7, 4],
        vec![0, 2, 5, 6, 7, 4],
        vec![0, 3, 5, 6, 7, 4],
        vec![0, 3, 6, 5, 7, 4],
        vec![0, 3, 6, 5, 1, 4],
        vec![0, 3, 6, 5, 2, 4],
        vec![0, 1, 5, 3, 6, 7, 4],
        vec![0, 2, 5, 3, 6, 7, 4],
        vec![0, 3, 6, 7, 5, 4],
        vec![0, 3, 6, 7, 5, 1, 4],
        vec![0, 3, 6, 7, 5, 2, 4]
    ]);

    assert_eq!(result, shared::traverse(&graph, 0, 4));
}

#[test]
fn test_traverse_b14() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![0, 3],
        vec![0, 1, 5, 3],
        vec![0, 2, 5, 3],
        vec![0, 1, 4, 5, 3],
        vec![0, 2, 4, 5, 3],
        vec![0, 1, 5, 6, 3],
        vec![0, 2, 5, 6, 3],
        vec![0, 1, 4, 5, 6, 3],
        vec![0, 2, 4, 5, 6, 3],
        vec![0, 2, 4, 1, 5, 3],
        vec![0, 1, 4, 2, 5, 3],
        vec![0, 2, 4, 1, 5, 6, 3],
        vec![0, 1, 4, 2, 5, 6, 3],
        vec![0, 1, 4, 7, 6, 3],
        vec![0, 2, 4, 7, 6, 3],
        vec![0, 1, 5, 7, 6, 3],
        vec![0, 2, 5, 7, 6, 3],
        vec![0, 1, 4, 5, 7, 6, 3],
        vec![0, 2, 4, 5, 7, 6, 3],
        vec![0, 1, 5, 4, 7, 6, 3],
        vec![0, 2, 5, 4, 7, 6, 3],
        vec![0, 2, 4, 1, 5, 7, 6, 3],
        vec![0, 1, 4, 2, 5, 7, 6, 3],
        vec![0, 1, 4, 7, 5, 3],
        vec![0, 2, 4, 7, 5, 3],
        vec![0, 1, 4, 7, 6, 5, 3],
        vec![0, 2, 4, 7, 6, 5, 3],
        vec![0, 2, 5, 1, 4, 7, 6, 3],
        vec![0, 1, 5, 2, 4, 7, 6, 3],
        vec![0, 1, 4, 7, 5, 6, 3],
        vec![0, 2, 4, 7, 5, 6, 3]
    ]);

    assert_eq!(result, shared::traverse(&graph, 0, 3));
}

#[test]
fn test_traverse_b13() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![0, 2],
        vec![0, 1, 4, 2],
        vec![0, 1, 5, 2],
        vec![0, 3, 5, 2],
        vec![0, 1, 4, 5, 2],
        vec![0, 1, 5, 4, 2],
        vec![0, 3, 5, 4, 2],
        vec![0, 3, 6, 5, 2],
        vec![0, 3, 5, 1, 4, 2],
        vec![0, 3, 6, 5, 4, 2],
        vec![0, 1, 5, 7, 4, 2],
        vec![0, 3, 5, 7, 4, 2],
        vec![0, 3, 6, 7, 4, 2],
        vec![0, 1, 5, 6, 7, 4, 2],
        vec![0, 3, 5, 6, 7, 4, 2],
        vec![0, 3, 6, 5, 7, 4, 2],
        vec![0, 3, 6, 5, 1, 4, 2],
        vec![0, 1, 4, 7, 5, 2],
        vec![0, 3, 6, 7, 5, 2],
        vec![0, 3, 6, 7, 4, 5, 2],
        vec![0, 1, 4, 7, 6, 5, 2],
        vec![0, 1, 4, 7, 6, 3, 5, 2],
        vec![0, 1, 5, 3, 6, 7, 4, 2],
        vec![0, 3, 6, 7, 5, 4, 2]
    ]);

    assert_eq!(result, shared::traverse(&graph, 0, 2));
}

#[test]
fn test_traverse_b12() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![0, 1],
        vec![0, 2, 4, 1],
        vec![0, 2, 5, 1],
        vec![0, 3, 5, 1],
        vec![0, 2, 4, 5, 1],
        vec![0, 2, 5, 4, 1],
        vec![0, 3, 5, 4, 1],
        vec![0, 3, 6, 5, 1],
        vec![0, 3, 5, 2, 4, 1],
        vec![0, 3, 6, 5, 4, 1],
        vec![0, 2, 5, 7, 4, 1],
        vec![0, 3, 5, 7, 4, 1],
        vec![0, 3, 6, 7, 4, 1],
        vec![0, 2, 5, 6, 7, 4, 1],
        vec![0, 3, 5, 6, 7, 4, 1],
        vec![0, 3, 6, 5, 7, 4, 1],
        vec![0, 3, 6, 5, 2, 4, 1],
        vec![0, 2, 4, 7, 5, 1],
        vec![0, 3, 6, 7, 5, 1],
        vec![0, 3, 6, 7, 4, 5, 1],
        vec![0, 2, 4, 7, 6, 5, 1],
        vec![0, 2, 4, 7, 6, 3, 5, 1],
        vec![0, 2, 5, 3, 6, 7, 4, 1],
        vec![0, 3, 6, 7, 5, 4, 1]
    ]);

    assert_eq!(result, shared::traverse(&graph, 0, 1));
}

#[test]
fn test_traverse_b28() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![1, 4, 7],
        vec![1, 5, 7],
        vec![1, 4, 5, 7],
        vec![1, 0, 2, 5, 7],
        vec![1, 4, 2, 5, 7],
        vec![1, 0, 3, 5, 7],
        vec![1, 5, 4, 7],
        vec![1, 0, 2, 4, 7],
        vec![1, 5, 2, 4, 7],
        vec![1, 0, 2, 5, 4, 7],
        vec![1, 0, 3, 5, 4, 7],
        vec![1, 0, 3, 5, 2, 4, 7],
        vec![1, 5, 6, 7],
        vec![1, 4, 5, 6, 7],
        vec![1, 0, 3, 6, 7],
        vec![1, 5, 3, 6, 7],
        vec![1, 4, 5, 3, 6, 7],
        vec![1, 0, 2, 5, 6, 7],
        vec![1, 4, 2, 5, 6, 7],
        vec![1, 0, 3, 5, 6, 7],
        vec![1, 0, 2, 5, 3, 6, 7],
        vec![1, 4, 2, 5, 3, 6, 7],
        vec![1, 0, 2, 4, 5, 7],
        vec![1, 0, 3, 6, 5, 7],
        vec![1, 0, 2, 4, 5, 6, 7],
        vec![1, 0, 3, 6, 5, 4, 7],
        vec![1, 4, 2, 0, 3, 5, 7],
        vec![1, 4, 2, 0, 3, 6, 7],
        vec![1, 5, 2, 0, 3, 6, 7],
        vec![1, 4, 5, 2, 0, 3, 6, 7],
        vec![1, 0, 2, 4, 5, 3, 6, 7],
        vec![1, 4, 2, 0, 3, 5, 6, 7]
    ]);

    assert_eq!(result, shared::traverse(&graph, 1, 7));
}

#[test]
fn test_traverse_b27() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![1, 5, 6],
        vec![1, 4, 5, 6],
        vec![1, 0, 3, 6],
        vec![1, 5, 3, 6],
        vec![1, 4, 5, 3, 6],
        vec![1, 0, 2, 5, 6],
        vec![1, 4, 2, 5, 6],
        vec![1, 0, 3, 5, 6],
        vec![1, 4, 7, 6],
        vec![1, 5, 7, 6],
        vec![1, 4, 5, 7, 6],
        vec![1, 0, 2, 5, 7, 6],
        vec![1, 4, 2, 5, 7, 6],
        vec![1, 0, 3, 5, 7, 6],
        vec![1, 0, 2, 5, 3, 6],
        vec![1, 4, 2, 5, 3, 6],
        vec![1, 5, 4, 7, 6],
        vec![1, 0, 2, 4, 7, 6],
        vec![1, 5, 2, 4, 7, 6],
        vec![1, 0, 2, 5, 4, 7, 6],
        vec![1, 0, 3, 5, 4, 7, 6],
        vec![1, 0, 3, 5, 2, 4, 7, 6],
        vec![1, 4, 7, 5, 6],
        vec![1, 0, 2, 4, 5, 6],
        vec![1, 0, 2, 4, 7, 5, 6],
        vec![1, 4, 2, 0, 3, 6],
        vec![1, 5, 2, 0, 3, 6],
        vec![1, 4, 5, 2, 0, 3, 6],
        vec![1, 4, 7, 5, 3, 6],
        vec![1, 0, 2, 4, 5, 3, 6],
        vec![1, 0, 2, 4, 7, 5, 3, 6],
        vec![1, 4, 2, 0, 3, 5, 6],
        vec![1, 0, 2, 4, 5, 7, 6],
        vec![1, 4, 2, 0, 3, 5, 7, 6]
    ]);

    assert_eq!(result, shared::traverse(&graph, 1, 6));
}

#[test]
fn test_traverse_b26() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![1, 5],
        vec![1, 4, 5],
        vec![1, 0, 2, 5],
        vec![1, 4, 2, 5],
        vec![1, 0, 3, 5],
        vec![1, 4, 7, 5],
        vec![1, 0, 2, 4, 5],
        vec![1, 0, 3, 6, 5],
        vec![1, 4, 7, 6, 5],
        vec![1, 0, 2, 4, 7, 5],
        vec![1, 0, 3, 6, 7, 5],
        vec![1, 0, 3, 6, 7, 4, 5],
        vec![1, 0, 2, 4, 7, 6, 5],
        vec![1, 4, 7, 6, 3, 5],
        vec![1, 4, 2, 0, 3, 5],
        vec![1, 0, 2, 4, 7, 6, 3, 5],
        vec![1, 0, 3, 6, 7, 4, 2, 5],
        vec![1, 4, 2, 0, 3, 6, 5]
    ]);

    assert_eq!(result, shared::traverse(&graph, 1, 5));
}

#[test]
fn test_traverse_b25() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![1, 4],
        vec![1, 5, 4],
        vec![1, 0, 2, 4],
        vec![1, 5, 2, 4],
        vec![1, 0, 2, 5, 4],
        vec![1, 0, 3, 5, 4],
        vec![1, 5, 7, 4],
        vec![1, 0, 2, 5, 7, 4],
        vec![1, 0, 3, 5, 7, 4],
        vec![1, 0, 3, 5, 2, 4],
        vec![1, 5, 6, 7, 4],
        vec![1, 0, 3, 6, 7, 4],
        vec![1, 5, 3, 6, 7, 4],
        vec![1, 0, 2, 5, 6, 7, 4],
        vec![1, 0, 3, 5, 6, 7, 4],
        vec![1, 0, 2, 5, 3, 6, 7, 4],
        vec![1, 0, 3, 6, 5, 4],
        vec![1, 0, 3, 6, 7, 5, 4],
        vec![1, 0, 3, 6, 5, 7, 4],
        vec![1, 5, 3, 0, 2, 4],
        vec![1, 0, 3, 6, 5, 2, 4],
        vec![1, 0, 3, 6, 7, 5, 2, 4]
    ]);

    assert_eq!(result, shared::traverse(&graph, 1, 4));
}

#[test]
fn test_traverse_b24() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![1, 0, 3],
        vec![1, 5, 3],
        vec![1, 4, 5, 3],
        vec![1, 0, 2, 5, 3],
        vec![1, 4, 2, 5, 3],
        vec![1, 5, 6, 3],
        vec![1, 4, 5, 6, 3],
        vec![1, 0, 2, 5, 6, 3],
        vec![1, 4, 2, 5, 6, 3],
        vec![1, 4, 7, 6, 3],
        vec![1, 5, 7, 6, 3],
        vec![1, 4, 5, 7, 6, 3],
        vec![1, 0, 2, 5, 7, 6, 3],
        vec![1, 4, 2, 5, 7, 6, 3],
        vec![1, 4, 2, 0, 3],
        vec![1, 5, 2, 0, 3],
        vec![1, 4, 5, 2, 0, 3],
        vec![1, 4, 7, 5, 3],
        vec![1, 0, 2, 4, 5, 3],
        vec![1, 4, 7, 6, 5, 3],
        vec![1, 0, 2, 4, 7, 5, 3],
        vec![1, 5, 4, 7, 6, 3],
        vec![1, 0, 2, 4, 7, 6, 3],
        vec![1, 5, 2, 4, 7, 6, 3],
        vec![1, 0, 2, 5, 4, 7, 6, 3],
        vec![1, 4, 7, 5, 6, 3],
        vec![1, 0, 2, 4, 5, 6, 3],
        vec![1, 0, 2, 4, 7, 5, 6, 3],
        vec![1, 0, 2, 4, 7, 6, 5, 3],
        vec![1, 0, 2, 4, 5, 7, 6, 3]
    ]);

    assert_eq!(result, shared::traverse(&graph, 1, 3));
}

#[test]
fn test_traverse_b23() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![1, 0, 2],
        vec![1, 4, 2],
        vec![1, 5, 2],
        vec![1, 4, 5, 2],
        vec![1, 0, 3, 5, 2],
        vec![1, 5, 4, 2],
        vec![1, 0, 3, 5, 4, 2],
        vec![1, 5, 7, 4, 2],
        vec![1, 0, 3, 5, 7, 4, 2],
        vec![1, 5, 3, 0, 2],
        vec![1, 4, 5, 3, 0, 2],
        vec![1, 5, 6, 7, 4, 2],
        vec![1, 0, 3, 6, 7, 4, 2],
        vec![1, 5, 3, 6, 7, 4, 2],
        vec![1, 0, 3, 5, 6, 7, 4, 2],
        vec![1, 4, 7, 5, 2],
        vec![1, 0, 3, 6, 5, 2],
        vec![1, 4, 7, 6, 5, 2],
        vec![1, 0, 3, 6, 7, 5, 2],
        vec![1, 0, 3, 6, 7, 4, 5, 2],
        vec![1, 0, 3, 6, 5, 4, 2],
        vec![1, 0, 3, 6, 7, 5, 4, 2],
        vec![1, 4, 7, 6, 3, 5, 2],
        vec![1, 5, 6, 3, 0, 2],
        vec![1, 4, 5, 6, 3, 0, 2],
        vec![1, 4, 7, 6, 3, 0, 2],
        vec![1, 5, 7, 6, 3, 0, 2],
        vec![1, 4, 5, 7, 6, 3, 0, 2],
        vec![1, 4, 7, 5, 3, 0, 2],
        vec![1, 4, 7, 6, 5, 3, 0, 2],
        vec![1, 5, 4, 7, 6, 3, 0, 2],
        vec![1, 4, 7, 5, 6, 3, 0, 2],
        vec![1, 0, 3, 6, 5, 7, 4, 2]
    ]);

    assert_eq!(result, shared::traverse(&graph, 1, 2));
}

#[test]
fn test_traverse_b21() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![1, 0],
        vec![1, 4, 2, 0],
        vec![1, 5, 2, 0],
        vec![1, 4, 5, 2, 0],
        vec![1, 5, 3, 0],
        vec![1, 4, 5, 3, 0],
        vec![1, 4, 2, 5, 3, 0],
        vec![1, 5, 6, 3, 0],
        vec![1, 4, 5, 6, 3, 0],
        vec![1, 4, 2, 5, 6, 3, 0],
        vec![1, 4, 7, 6, 3, 0],
        vec![1, 5, 7, 6, 3, 0],
        vec![1, 4, 5, 7, 6, 3, 0],
        vec![1, 4, 2, 5, 7, 6, 3, 0],
        vec![1, 4, 7, 5, 3, 0],
        vec![1, 4, 7, 6, 5, 3, 0],
        vec![1, 5, 4, 7, 6, 3, 0],
        vec![1, 5, 2, 4, 7, 6, 3, 0],
        vec![1, 4, 7, 5, 6, 3, 0],
        vec![1, 5, 4, 2, 0],
        vec![1, 5, 7, 4, 2, 0],
        vec![1, 5, 6, 7, 4, 2, 0],
        vec![1, 5, 3, 6, 7, 4, 2, 0],
        vec![1, 4, 7, 5, 2, 0],
        vec![1, 4, 7, 6, 5, 2, 0],
        vec![1, 4, 7, 6, 3, 5, 2, 0]
    ]);

    assert_eq!(result, shared::traverse(&graph, 1, 0));
}

#[test]
fn test_traverse_b38() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![2, 4, 7],
        vec![2, 5, 7],
        vec![2, 4, 5, 7],
        vec![2, 0, 1, 5, 7],
        vec![2, 4, 1, 5, 7],
        vec![2, 0, 3, 5, 7],
        vec![2, 5, 4, 7],
        vec![2, 0, 1, 4, 7],
        vec![2, 5, 1, 4, 7],
        vec![2, 0, 1, 5, 4, 7],
        vec![2, 0, 3, 5, 4, 7],
        vec![2, 0, 3, 5, 1, 4, 7],
        vec![2, 5, 6, 7],
        vec![2, 4, 5, 6, 7],
        vec![2, 0, 3, 6, 7],
        vec![2, 5, 3, 6, 7],
        vec![2, 4, 5, 3, 6, 7],
        vec![2, 0, 1, 5, 6, 7],
        vec![2, 4, 1, 5, 6, 7],
        vec![2, 0, 3, 5, 6, 7],
        vec![2, 0, 1, 5, 3, 6, 7],
        vec![2, 4, 1, 5, 3, 6, 7],
        vec![2, 0, 1, 4, 5, 7],
        vec![2, 0, 3, 6, 5, 7],
        vec![2, 0, 1, 4, 5, 6, 7],
        vec![2, 0, 3, 6, 5, 4, 7],
        vec![2, 4, 1, 0, 3, 5, 7],
        vec![2, 4, 1, 0, 3, 6, 7],
        vec![2, 5, 1, 0, 3, 6, 7],
        vec![2, 4, 5, 1, 0, 3, 6, 7],
        vec![2, 0, 1, 4, 5, 3, 6, 7],
        vec![2, 4, 1, 0, 3, 5, 6, 7]
    ]);

    assert_eq!(result, shared::traverse(&graph, 2, 7));
}

#[test]
fn test_traverse_b37() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![2, 5, 6],
        vec![2, 4, 5, 6],
        vec![2, 0, 3, 6],
        vec![2, 5, 3, 6],
        vec![2, 4, 5, 3, 6],
        vec![2, 0, 1, 5, 6],
        vec![2, 4, 1, 5, 6],
        vec![2, 0, 3, 5, 6],
        vec![2, 4, 7, 6],
        vec![2, 5, 7, 6],
        vec![2, 4, 5, 7, 6],
        vec![2, 0, 1, 5, 7, 6],
        vec![2, 4, 1, 5, 7, 6],
        vec![2, 0, 3, 5, 7, 6],
        vec![2, 0, 1, 5, 3, 6],
        vec![2, 4, 1, 5, 3, 6],
        vec![2, 5, 4, 7, 6],
        vec![2, 0, 1, 4, 7, 6],
        vec![2, 5, 1, 4, 7, 6],
        vec![2, 0, 1, 5, 4, 7, 6],
        vec![2, 0, 3, 5, 4, 7, 6],
        vec![2, 0, 3, 5, 1, 4, 7, 6],
        vec![2, 4, 7, 5, 6],
        vec![2, 0, 1, 4, 5, 6],
        vec![2, 0, 1, 4, 7, 5, 6],
        vec![2, 4, 1, 0, 3, 6],
        vec![2, 5, 1, 0, 3, 6],
        vec![2, 4, 5, 1, 0, 3, 6],
        vec![2, 4, 7, 5, 3, 6],
        vec![2, 0, 1, 4, 5, 3, 6],
        vec![2, 0, 1, 4, 7, 5, 3, 6],
        vec![2, 4, 1, 0, 3, 5, 6],
        vec![2, 0, 1, 4, 5, 7, 6],
        vec![2, 4, 1, 0, 3, 5, 7, 6]
    ]);

    assert_eq!(result, shared::traverse(&graph, 2, 6));
}

#[test]
fn test_traverse_b36() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![2, 5],
        vec![2, 4, 5],
        vec![2, 0, 1, 5],
        vec![2, 4, 1, 5],
        vec![2, 0, 3, 5],
        vec![2, 4, 7, 5],
        vec![2, 0, 1, 4, 5],
        vec![2, 0, 3, 6, 5],
        vec![2, 4, 7, 6, 5],
        vec![2, 0, 1, 4, 7, 5],
        vec![2, 0, 3, 6, 7, 5],
        vec![2, 0, 3, 6, 7, 4, 5],
        vec![2, 0, 1, 4, 7, 6, 5],
        vec![2, 4, 7, 6, 3, 5],
        vec![2, 4, 1, 0, 3, 5],
        vec![2, 0, 1, 4, 7, 6, 3, 5],
        vec![2, 0, 3, 6, 7, 4, 1, 5],
        vec![2, 4, 1, 0, 3, 6, 5]
    ]);

    assert_eq!(result, shared::traverse(&graph, 2, 5));
}

#[test]
fn test_traverse_b35() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![2, 4],
        vec![2, 5, 4],
        vec![2, 0, 1, 4],
        vec![2, 5, 1, 4],
        vec![2, 0, 1, 5, 4],
        vec![2, 0, 3, 5, 4],
        vec![2, 5, 7, 4],
        vec![2, 0, 1, 5, 7, 4],
        vec![2, 0, 3, 5, 7, 4],
        vec![2, 0, 3, 5, 1, 4],
        vec![2, 5, 6, 7, 4],
        vec![2, 0, 3, 6, 7, 4],
        vec![2, 5, 3, 6, 7, 4],
        vec![2, 0, 1, 5, 6, 7, 4],
        vec![2, 0, 3, 5, 6, 7, 4],
        vec![2, 0, 1, 5, 3, 6, 7, 4],
        vec![2, 0, 3, 6, 5, 4],
        vec![2, 0, 3, 6, 7, 5, 4],
        vec![2, 0, 3, 6, 5, 7, 4],
        vec![2, 5, 3, 0, 1, 4],
        vec![2, 0, 3, 6, 5, 1, 4],
        vec![2, 0, 3, 6, 7, 5, 1, 4]
    ]);

    assert_eq!(result, shared::traverse(&graph, 2, 4));
}

#[test]
fn test_traverse_b34() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![2, 0, 3],
        vec![2, 5, 3],
        vec![2, 4, 5, 3],
        vec![2, 0, 1, 5, 3],
        vec![2, 4, 1, 5, 3],
        vec![2, 5, 6, 3],
        vec![2, 4, 5, 6, 3],
        vec![2, 0, 1, 5, 6, 3],
        vec![2, 4, 1, 5, 6, 3],
        vec![2, 4, 7, 6, 3],
        vec![2, 5, 7, 6, 3],
        vec![2, 4, 5, 7, 6, 3],
        vec![2, 0, 1, 5, 7, 6, 3],
        vec![2, 4, 1, 5, 7, 6, 3],
        vec![2, 4, 1, 0, 3],
        vec![2, 5, 1, 0, 3],
        vec![2, 4, 5, 1, 0, 3],
        vec![2, 4, 7, 5, 3],
        vec![2, 0, 1, 4, 5, 3],
        vec![2, 4, 7, 6, 5, 3],
        vec![2, 0, 1, 4, 7, 5, 3],
        vec![2, 5, 4, 7, 6, 3],
        vec![2, 0, 1, 4, 7, 6, 3],
        vec![2, 5, 1, 4, 7, 6, 3],
        vec![2, 0, 1, 5, 4, 7, 6, 3],
        vec![2, 4, 7, 5, 6, 3],
        vec![2, 0, 1, 4, 5, 6, 3],
        vec![2, 0, 1, 4, 7, 5, 6, 3],
        vec![2, 0, 1, 4, 7, 6, 5, 3],
        vec![2, 0, 1, 4, 5, 7, 6, 3]
    ]);

    assert_eq!(result, shared::traverse(&graph, 2, 3));
}

#[test]
fn test_traverse_b32() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![2, 0, 1],
        vec![2, 4, 1],
        vec![2, 5, 1],
        vec![2, 4, 5, 1],
        vec![2, 0, 3, 5, 1],
        vec![2, 5, 4, 1],
        vec![2, 0, 3, 5, 4, 1],
        vec![2, 5, 7, 4, 1],
        vec![2, 0, 3, 5, 7, 4, 1],
        vec![2, 5, 3, 0, 1],
        vec![2, 4, 5, 3, 0, 1],
        vec![2, 5, 6, 7, 4, 1],
        vec![2, 0, 3, 6, 7, 4, 1],
        vec![2, 5, 3, 6, 7, 4, 1],
        vec![2, 0, 3, 5, 6, 7, 4, 1],
        vec![2, 4, 7, 5, 1],
        vec![2, 0, 3, 6, 5, 1],
        vec![2, 4, 7, 6, 5, 1],
        vec![2, 0, 3, 6, 7, 5, 1],
        vec![2, 0, 3, 6, 7, 4, 5, 1],
        vec![2, 0, 3, 6, 5, 4, 1],
        vec![2, 0, 3, 6, 7, 5, 4, 1],
        vec![2, 4, 7, 6, 3, 5, 1],
        vec![2, 5, 6, 3, 0, 1],
        vec![2, 4, 5, 6, 3, 0, 1],
        vec![2, 4, 7, 6, 3, 0, 1],
        vec![2, 5, 7, 6, 3, 0, 1],
        vec![2, 4, 5, 7, 6, 3, 0, 1],
        vec![2, 4, 7, 5, 3, 0, 1],
        vec![2, 4, 7, 6, 5, 3, 0, 1],
        vec![2, 5, 4, 7, 6, 3, 0, 1],
        vec![2, 4, 7, 5, 6, 3, 0, 1],
        vec![2, 0, 3, 6, 5, 7, 4, 1]
    ]);

    assert_eq!(result, shared::traverse(&graph, 2, 1));
}

#[test]
fn test_traverse_b31() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![2, 0],
        vec![2, 4, 1, 0],
        vec![2, 5, 1, 0],
        vec![2, 4, 5, 1, 0],
        vec![2, 5, 3, 0],
        vec![2, 4, 5, 3, 0],
        vec![2, 4, 1, 5, 3, 0],
        vec![2, 5, 6, 3, 0],
        vec![2, 4, 5, 6, 3, 0],
        vec![2, 4, 1, 5, 6, 3, 0],
        vec![2, 4, 7, 6, 3, 0],
        vec![2, 5, 7, 6, 3, 0],
        vec![2, 4, 5, 7, 6, 3, 0],
        vec![2, 4, 1, 5, 7, 6, 3, 0],
        vec![2, 4, 7, 5, 3, 0],
        vec![2, 4, 7, 6, 5, 3, 0],
        vec![2, 5, 4, 7, 6, 3, 0],
        vec![2, 5, 1, 4, 7, 6, 3, 0],
        vec![2, 4, 7, 5, 6, 3, 0],
        vec![2, 5, 4, 1, 0],
        vec![2, 5, 7, 4, 1, 0],
        vec![2, 5, 6, 7, 4, 1, 0],
        vec![2, 5, 3, 6, 7, 4, 1, 0],
        vec![2, 4, 7, 5, 1, 0],
        vec![2, 4, 7, 6, 5, 1, 0],
        vec![2, 4, 7, 6, 3, 5, 1, 0]
    ]);

    assert_eq!(result, shared::traverse(&graph, 2, 0));
}

#[test]
fn test_traverse_b48() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![3, 5, 7],
        vec![3, 6, 7],
        vec![3, 5, 6, 7],
        vec![3, 5, 4, 7],
        vec![3, 0, 1, 4, 7],
        vec![3, 5, 1, 4, 7],
        vec![3, 0, 2, 4, 7],
        vec![3, 5, 2, 4, 7],
        vec![3, 6, 5, 7],
        vec![3, 0, 1, 5, 7],
        vec![3, 0, 2, 5, 7],
        vec![3, 0, 1, 4, 5, 7],
        vec![3, 0, 2, 4, 5, 7],
        vec![3, 6, 5, 4, 7],
        vec![3, 0, 1, 5, 4, 7],
        vec![3, 0, 2, 5, 4, 7],
        vec![3, 0, 2, 4, 1, 5, 7],
        vec![3, 0, 1, 4, 2, 5, 7],
        vec![3, 6, 5, 1, 4, 7],
        vec![3, 0, 2, 5, 1, 4, 7],
        vec![3, 5, 2, 0, 1, 4, 7],
        vec![3, 6, 5, 2, 4, 7],
        vec![3, 0, 1, 5, 2, 4, 7],
        vec![3, 5, 1, 0, 2, 4, 7],
        vec![3, 0, 1, 5, 6, 7],
        vec![3, 0, 2, 5, 6, 7],
        vec![3, 0, 1, 4, 5, 6, 7],
        vec![3, 0, 2, 4, 5, 6, 7],
        vec![3, 0, 2, 4, 1, 5, 6, 7],
        vec![3, 0, 1, 4, 2, 5, 6, 7]
    ]);

    assert_eq!(result, shared::traverse(&graph, 3, 7));
}

#[test]
fn test_traverse_b47() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![3, 6],
        vec![3, 5, 6],
        vec![3, 5, 7, 6],
        vec![3, 5, 4, 7, 6],
        vec![3, 0, 1, 4, 7, 6],
        vec![3, 5, 1, 4, 7, 6],
        vec![3, 0, 2, 4, 7, 6],
        vec![3, 5, 2, 4, 7, 6],
        vec![3, 0, 1, 5, 6],
        vec![3, 0, 2, 5, 6],
        vec![3, 0, 1, 4, 5, 6],
        vec![3, 0, 2, 4, 5, 6],
        vec![3, 0, 1, 4, 7, 5, 6],
        vec![3, 0, 2, 4, 7, 5, 6],
        vec![3, 0, 1, 5, 7, 6],
        vec![3, 0, 2, 5, 7, 6],
        vec![3, 0, 1, 4, 5, 7, 6],
        vec![3, 0, 2, 4, 5, 7, 6],
        vec![3, 0, 2, 4, 1, 5, 6],
        vec![3, 0, 1, 4, 2, 5, 6],
        vec![3, 0, 1, 5, 4, 7, 6],
        vec![3, 0, 2, 5, 4, 7, 6],
        vec![3, 0, 2, 4, 1, 5, 7, 6],
        vec![3, 0, 1, 4, 2, 5, 7, 6],
        vec![3, 0, 2, 5, 1, 4, 7, 6],
        vec![3, 5, 2, 0, 1, 4, 7, 6],
        vec![3, 0, 1, 5, 2, 4, 7, 6],
        vec![3, 5, 1, 0, 2, 4, 7, 6]
    ]);

    assert_eq!(result, shared::traverse(&graph, 3, 6));
}

#[test]
fn test_traverse_b46() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![3, 5],
        vec![3, 6, 5],
        vec![3, 0, 1, 5],
        vec![3, 0, 2, 5],
        vec![3, 0, 1, 4, 5],
        vec![3, 0, 2, 4, 5],
        vec![3, 6, 7, 5],
        vec![3, 0, 1, 4, 7, 5],
        vec![3, 0, 2, 4, 7, 5],
        vec![3, 6, 7, 4, 5],
        vec![3, 0, 2, 4, 1, 5],
        vec![3, 6, 7, 4, 1, 5],
        vec![3, 0, 1, 4, 2, 5],
        vec![3, 6, 7, 4, 2, 5],
        vec![3, 0, 1, 4, 7, 6, 5],
        vec![3, 0, 2, 4, 7, 6, 5]
    ]);

    assert_eq!(result, shared::traverse(&graph, 3, 5));
}

#[test]
fn test_traverse_b45() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![3, 5, 4],
        vec![3, 0, 1, 4],
        vec![3, 5, 1, 4],
        vec![3, 0, 2, 4],
        vec![3, 5, 2, 4],
        vec![3, 5, 7, 4],
        vec![3, 6, 7, 4],
        vec![3, 5, 6, 7, 4],
        vec![3, 6, 5, 4],
        vec![3, 0, 1, 5, 4],
        vec![3, 0, 2, 5, 4],
        vec![3, 6, 7, 5, 4],
        vec![3, 6, 5, 7, 4],
        vec![3, 0, 1, 5, 7, 4],
        vec![3, 0, 2, 5, 7, 4],
        vec![3, 6, 5, 1, 4],
        vec![3, 0, 2, 5, 1, 4],
        vec![3, 6, 7, 5, 1, 4],
        vec![3, 5, 2, 0, 1, 4],
        vec![3, 6, 5, 2, 4],
        vec![3, 0, 1, 5, 2, 4],
        vec![3, 6, 7, 5, 2, 4],
        vec![3, 5, 1, 0, 2, 4],
        vec![3, 0, 1, 5, 6, 7, 4],
        vec![3, 0, 2, 5, 6, 7, 4]
    ]);

    assert_eq!(result, shared::traverse(&graph, 3, 4));
}

#[test]
fn test_traverse_b43() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![3, 0, 2],
        vec![3, 5, 2],
        vec![3, 5, 4, 2],
        vec![3, 0, 1, 4, 2],
        vec![3, 5, 1, 4, 2],
        vec![3, 6, 5, 2],
        vec![3, 0, 1, 5, 2],
        vec![3, 0, 1, 4, 5, 2],
        vec![3, 6, 7, 5, 2],
        vec![3, 0, 1, 4, 7, 5, 2],
        vec![3, 5, 1, 0, 2],
        vec![3, 5, 7, 4, 2],
        vec![3, 6, 7, 4, 2],
        vec![3, 5, 6, 7, 4, 2],
        vec![3, 6, 5, 4, 2],
        vec![3, 0, 1, 5, 4, 2],
        vec![3, 6, 7, 5, 4, 2],
        vec![3, 6, 5, 7, 4, 2],
        vec![3, 0, 1, 5, 7, 4, 2],
        vec![3, 6, 7, 4, 5, 2],
        vec![3, 6, 7, 4, 1, 5, 2],
        vec![3, 6, 5, 1, 4, 2],
        vec![3, 6, 7, 5, 1, 4, 2],
        vec![3, 5, 4, 1, 0, 2],
        vec![3, 6, 5, 1, 0, 2],
        vec![3, 6, 7, 5, 1, 0, 2],
        vec![3, 5, 7, 4, 1, 0, 2],
        vec![3, 6, 7, 4, 1, 0, 2],
        vec![3, 5, 6, 7, 4, 1, 0, 2],
        vec![3, 6, 5, 4, 1, 0, 2],
        vec![3, 6, 7, 5, 4, 1, 0, 2],
        vec![3, 6, 5, 7, 4, 1, 0, 2],
        vec![3, 6, 7, 4, 5, 1, 0, 2],
        vec![3, 0, 1, 4, 7, 6, 5, 2]
    ]);

    assert_eq!(result, shared::traverse(&graph, 3, 2));
}

#[test]
fn test_traverse_b42() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![3, 0, 1],
        vec![3, 5, 1],
        vec![3, 5, 4, 1],
        vec![3, 0, 2, 4, 1],
        vec![3, 5, 2, 4, 1],
        vec![3, 6, 5, 1],
        vec![3, 0, 2, 5, 1],
        vec![3, 0, 2, 4, 5, 1],
        vec![3, 6, 7, 5, 1],
        vec![3, 0, 2, 4, 7, 5, 1],
        vec![3, 5, 2, 0, 1],
        vec![3, 5, 7, 4, 1],
        vec![3, 6, 7, 4, 1],
        vec![3, 5, 6, 7, 4, 1],
        vec![3, 6, 5, 4, 1],
        vec![3, 0, 2, 5, 4, 1],
        vec![3, 6, 7, 5, 4, 1],
        vec![3, 6, 5, 7, 4, 1],
        vec![3, 0, 2, 5, 7, 4, 1],
        vec![3, 6, 7, 4, 5, 1],
        vec![3, 6, 7, 4, 2, 5, 1],
        vec![3, 6, 5, 2, 4, 1],
        vec![3, 6, 7, 5, 2, 4, 1],
        vec![3, 5, 4, 2, 0, 1],
        vec![3, 6, 5, 2, 0, 1],
        vec![3, 6, 7, 5, 2, 0, 1],
        vec![3, 5, 7, 4, 2, 0, 1],
        vec![3, 6, 7, 4, 2, 0, 1],
        vec![3, 5, 6, 7, 4, 2, 0, 1],
        vec![3, 6, 5, 4, 2, 0, 1],
        vec![3, 6, 7, 5, 4, 2, 0, 1],
        vec![3, 6, 5, 7, 4, 2, 0, 1],
        vec![3, 6, 7, 4, 5, 2, 0, 1],
        vec![3, 0, 2, 4, 7, 6, 5, 1]
    ]);

    assert_eq!(result, shared::traverse(&graph, 3, 1));
}

#[test]
fn test_traverse_b41() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![3, 0],
        vec![3, 5, 1, 0],
        vec![3, 5, 2, 0],
        vec![3, 5, 4, 1, 0],
        vec![3, 5, 2, 4, 1, 0],
        vec![3, 6, 5, 1, 0],
        vec![3, 6, 7, 5, 1, 0],
        vec![3, 5, 7, 4, 1, 0],
        vec![3, 6, 7, 4, 1, 0],
        vec![3, 5, 6, 7, 4, 1, 0],
        vec![3, 6, 5, 4, 1, 0],
        vec![3, 6, 7, 5, 4, 1, 0],
        vec![3, 6, 5, 7, 4, 1, 0],
        vec![3, 6, 7, 4, 5, 1, 0],
        vec![3, 5, 4, 2, 0],
        vec![3, 5, 1, 4, 2, 0],
        vec![3, 6, 5, 2, 0],
        vec![3, 6, 7, 5, 2, 0],
        vec![3, 5, 7, 4, 2, 0],
        vec![3, 6, 7, 4, 2, 0],
        vec![3, 5, 6, 7, 4, 2, 0],
        vec![3, 6, 5, 4, 2, 0],
        vec![3, 6, 7, 5, 4, 2, 0],
        vec![3, 6, 5, 7, 4, 2, 0],
        vec![3, 6, 7, 4, 5, 2, 0],
        vec![3, 6, 7, 4, 1, 5, 2, 0],
        vec![3, 6, 7, 4, 2, 5, 1, 0],
        vec![3, 6, 5, 1, 4, 2, 0],
        vec![3, 6, 7, 5, 1, 4, 2, 0],
        vec![3, 6, 5, 2, 4, 1, 0],
        vec![3, 6, 7, 5, 2, 4, 1, 0]
    ]);

    assert_eq!(result, shared::traverse(&graph, 3, 0));
}

#[test]
fn test_traverse_b58() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![4, 7],
        vec![4, 5, 7],
        vec![4, 1, 5, 7],
        vec![4, 2, 5, 7],
        vec![4, 5, 6, 7],
        vec![4, 1, 5, 6, 7],
        vec![4, 2, 5, 6, 7],
        vec![4, 5, 3, 6, 7],
        vec![4, 1, 5, 3, 6, 7],
        vec![4, 2, 5, 3, 6, 7],
        vec![4, 1, 0, 3, 6, 7],
        vec![4, 2, 0, 3, 6, 7],
        vec![4, 2, 0, 1, 5, 7],
        vec![4, 1, 0, 2, 5, 7],
        vec![4, 1, 0, 3, 5, 7],
        vec![4, 2, 0, 3, 5, 7],
        vec![4, 1, 0, 3, 6, 5, 7],
        vec![4, 2, 0, 3, 6, 5, 7],
        vec![4, 2, 0, 1, 5, 6, 7],
        vec![4, 1, 0, 2, 5, 6, 7],
        vec![4, 1, 0, 3, 5, 6, 7],
        vec![4, 2, 0, 3, 5, 6, 7],
        vec![4, 2, 0, 1, 5, 3, 6, 7],
        vec![4, 1, 0, 2, 5, 3, 6, 7],
        vec![4, 5, 1, 0, 3, 6, 7],
        vec![4, 2, 5, 1, 0, 3, 6, 7],
        vec![4, 5, 2, 0, 3, 6, 7],
        vec![4, 1, 5, 2, 0, 3, 6, 7]
    ]);

    assert_eq!(result, shared::traverse(&graph, 4, 7));
}

#[test]
fn test_traverse_b57() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![4, 5, 6],
        vec![4, 1, 5, 6],
        vec![4, 2, 5, 6],
        vec![4, 7, 6],
        vec![4, 5, 7, 6],
        vec![4, 1, 5, 7, 6],
        vec![4, 2, 5, 7, 6],
        vec![4, 7, 5, 6],
        vec![4, 5, 3, 6],
        vec![4, 1, 5, 3, 6],
        vec![4, 2, 5, 3, 6],
        vec![4, 1, 0, 3, 6],
        vec![4, 2, 0, 3, 6],
        vec![4, 7, 5, 3, 6],
        vec![4, 2, 0, 1, 5, 6],
        vec![4, 1, 0, 2, 5, 6],
        vec![4, 1, 0, 3, 5, 6],
        vec![4, 2, 0, 3, 5, 6],
        vec![4, 2, 0, 1, 5, 3, 6],
        vec![4, 1, 0, 2, 5, 3, 6],
        vec![4, 5, 1, 0, 3, 6],
        vec![4, 2, 5, 1, 0, 3, 6],
        vec![4, 7, 5, 1, 0, 3, 6],
        vec![4, 5, 2, 0, 3, 6],
        vec![4, 1, 5, 2, 0, 3, 6],
        vec![4, 7, 5, 2, 0, 3, 6],
        vec![4, 2, 0, 1, 5, 7, 6],
        vec![4, 1, 0, 2, 5, 7, 6],
        vec![4, 1, 0, 3, 5, 7, 6],
        vec![4, 2, 0, 3, 5, 7, 6]
    ]);

    assert_eq!(result, shared::traverse(&graph, 4, 6));
}

#[test]
fn test_traverse_b56() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![4, 5],
        vec![4, 1, 5],
        vec![4, 2, 5],
        vec![4, 7, 5],
        vec![4, 2, 0, 1, 5],
        vec![4, 1, 0, 2, 5],
        vec![4, 1, 0, 3, 5],
        vec![4, 2, 0, 3, 5],
        vec![4, 7, 6, 5],
        vec![4, 1, 0, 3, 6, 5],
        vec![4, 2, 0, 3, 6, 5],
        vec![4, 1, 0, 3, 6, 7, 5],
        vec![4, 2, 0, 3, 6, 7, 5],
        vec![4, 7, 6, 3, 5],
        vec![4, 7, 6, 3, 0, 1, 5],
        vec![4, 7, 6, 3, 0, 2, 5]
    ]);

    assert_eq!(result, shared::traverse(&graph, 4, 5));
}

#[test]
fn test_traverse_b54() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![4, 5, 3],
        vec![4, 1, 5, 3],
        vec![4, 2, 5, 3],
        vec![4, 1, 0, 3],
        vec![4, 2, 0, 3],
        vec![4, 7, 5, 3],
        vec![4, 5, 6, 3],
        vec![4, 1, 5, 6, 3],
        vec![4, 2, 5, 6, 3],
        vec![4, 7, 6, 3],
        vec![4, 5, 7, 6, 3],
        vec![4, 1, 5, 7, 6, 3],
        vec![4, 2, 5, 7, 6, 3],
        vec![4, 7, 5, 6, 3],
        vec![4, 2, 0, 1, 5, 3],
        vec![4, 1, 0, 2, 5, 3],
        vec![4, 7, 6, 5, 3],
        vec![4, 2, 0, 1, 5, 6, 3],
        vec![4, 1, 0, 2, 5, 6, 3],
        vec![4, 5, 1, 0, 3],
        vec![4, 2, 5, 1, 0, 3],
        vec![4, 7, 5, 1, 0, 3],
        vec![4, 5, 2, 0, 3],
        vec![4, 1, 5, 2, 0, 3],
        vec![4, 7, 5, 2, 0, 3],
        vec![4, 7, 6, 5, 1, 0, 3],
        vec![4, 7, 6, 5, 2, 0, 3],
        vec![4, 2, 0, 1, 5, 7, 6, 3],
        vec![4, 1, 0, 2, 5, 7, 6, 3]
    ]);

    assert_eq!(result, shared::traverse(&graph, 4, 3));
}

#[test]
fn test_traverse_b53() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![4, 2],
        vec![4, 5, 2],
        vec![4, 1, 5, 2],
        vec![4, 1, 0, 2],
        vec![4, 7, 5, 2],
        vec![4, 1, 0, 3, 5, 2],
        vec![4, 7, 6, 5, 2],
        vec![4, 1, 0, 3, 6, 5, 2],
        vec![4, 1, 0, 3, 6, 7, 5, 2],
        vec![4, 5, 1, 0, 2],
        vec![4, 7, 5, 1, 0, 2],
        vec![4, 5, 3, 0, 2],
        vec![4, 1, 5, 3, 0, 2],
        vec![4, 7, 5, 3, 0, 2],
        vec![4, 7, 6, 5, 1, 0, 2],
        vec![4, 5, 6, 3, 0, 2],
        vec![4, 1, 5, 6, 3, 0, 2],
        vec![4, 7, 6, 3, 0, 2],
        vec![4, 5, 7, 6, 3, 0, 2],
        vec![4, 1, 5, 7, 6, 3, 0, 2],
        vec![4, 7, 5, 6, 3, 0, 2],
        vec![4, 7, 6, 5, 3, 0, 2],
        vec![4, 7, 6, 3, 5, 2]
    ]);

    assert_eq!(result, shared::traverse(&graph, 4, 2));
}

#[test]
fn test_traverse_b52() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![4, 1],
        vec![4, 5, 1],
        vec![4, 2, 5, 1],
        vec![4, 2, 0, 1],
        vec![4, 7, 5, 1],
        vec![4, 2, 0, 3, 5, 1],
        vec![4, 7, 6, 5, 1],
        vec![4, 2, 0, 3, 6, 5, 1],
        vec![4, 2, 0, 3, 6, 7, 5, 1],
        vec![4, 5, 2, 0, 1],
        vec![4, 7, 5, 2, 0, 1],
        vec![4, 5, 3, 0, 1],
        vec![4, 2, 5, 3, 0, 1],
        vec![4, 7, 5, 3, 0, 1],
        vec![4, 7, 6, 5, 2, 0, 1],
        vec![4, 5, 6, 3, 0, 1],
        vec![4, 2, 5, 6, 3, 0, 1],
        vec![4, 7, 6, 3, 0, 1],
        vec![4, 5, 7, 6, 3, 0, 1],
        vec![4, 2, 5, 7, 6, 3, 0, 1],
        vec![4, 7, 5, 6, 3, 0, 1],
        vec![4, 7, 6, 5, 3, 0, 1],
        vec![4, 7, 6, 3, 5, 1]
    ]);

    assert_eq!(result, shared::traverse(&graph, 4, 1));
}

#[test]
fn test_traverse_b51() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![4, 1, 0],
        vec![4, 2, 0],
        vec![4, 5, 1, 0],
        vec![4, 2, 5, 1, 0],
        vec![4, 7, 5, 1, 0],
        vec![4, 5, 2, 0],
        vec![4, 1, 5, 2, 0],
        vec![4, 7, 5, 2, 0],
        vec![4, 5, 3, 0],
        vec![4, 1, 5, 3, 0],
        vec![4, 2, 5, 3, 0],
        vec![4, 7, 5, 3, 0],
        vec![4, 7, 6, 5, 1, 0],
        vec![4, 7, 6, 5, 2, 0],
        vec![4, 5, 6, 3, 0],
        vec![4, 1, 5, 6, 3, 0],
        vec![4, 2, 5, 6, 3, 0],
        vec![4, 7, 6, 3, 0],
        vec![4, 5, 7, 6, 3, 0],
        vec![4, 1, 5, 7, 6, 3, 0],
        vec![4, 2, 5, 7, 6, 3, 0],
        vec![4, 7, 5, 6, 3, 0],
        vec![4, 7, 6, 5, 3, 0],
        vec![4, 7, 6, 3, 5, 1, 0],
        vec![4, 7, 6, 3, 5, 2, 0]
    ]);

    assert_eq!(result, shared::traverse(&graph, 4, 0));
}

#[test]
fn test_traverse_b68() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![5, 7],
        vec![5, 4, 7],
        vec![5, 1, 4, 7],
        vec![5, 2, 4, 7],
        vec![5, 6, 7],
        vec![5, 3, 6, 7],
        vec![5, 2, 0, 1, 4, 7],
        vec![5, 3, 0, 1, 4, 7],
        vec![5, 1, 0, 2, 4, 7],
        vec![5, 3, 0, 2, 4, 7],
        vec![5, 1, 0, 3, 6, 7],
        vec![5, 2, 0, 3, 6, 7]
    ]);

    assert_eq!(result, shared::traverse(&graph, 5, 7));
}

#[test]
fn test_traverse_b67() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![5, 6],
        vec![5, 3, 6],
        vec![5, 7, 6],
        vec![5, 4, 7, 6],
        vec![5, 1, 4, 7, 6],
        vec![5, 2, 4, 7, 6],
        vec![5, 1, 0, 3, 6],
        vec![5, 2, 0, 3, 6],
        vec![5, 2, 0, 1, 4, 7, 6],
        vec![5, 3, 0, 1, 4, 7, 6],
        vec![5, 1, 0, 2, 4, 7, 6],
        vec![5, 3, 0, 2, 4, 7, 6]
    ]);

    assert_eq!(result, shared::traverse(&graph, 5, 6));
}

#[test]
fn test_traverse_b65() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![5, 4],
        vec![5, 1, 4],
        vec![5, 2, 4],
        vec![5, 7, 4],
        vec![5, 6, 7, 4],
        vec![5, 3, 6, 7, 4],
        vec![5, 2, 0, 1, 4],
        vec![5, 3, 0, 1, 4],
        vec![5, 1, 0, 2, 4],
        vec![5, 3, 0, 2, 4],
        vec![5, 1, 0, 3, 6, 7, 4],
        vec![5, 2, 0, 3, 6, 7, 4]
    ]);

    assert_eq!(result, shared::traverse(&graph, 5, 4));
}

#[test]
fn test_traverse_b64() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![5, 3],
        vec![5, 6, 3],
        vec![5, 1, 0, 3],
        vec![5, 2, 0, 3],
        vec![5, 7, 6, 3],
        vec![5, 4, 7, 6, 3],
        vec![5, 1, 4, 7, 6, 3],
        vec![5, 2, 4, 7, 6, 3],
        vec![5, 4, 1, 0, 3],
        vec![5, 2, 4, 1, 0, 3],
        vec![5, 7, 4, 1, 0, 3],
        vec![5, 6, 7, 4, 1, 0, 3],
        vec![5, 4, 2, 0, 3],
        vec![5, 1, 4, 2, 0, 3],
        vec![5, 7, 4, 2, 0, 3],
        vec![5, 6, 7, 4, 2, 0, 3]
    ]);

    assert_eq!(result, shared::traverse(&graph, 5, 3));
}

#[test]
fn test_traverse_b63() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![5, 2],
        vec![5, 4, 2],
        vec![5, 1, 4, 2],
        vec![5, 1, 0, 2],
        vec![5, 3, 0, 2],
        vec![5, 7, 4, 2],
        vec![5, 6, 7, 4, 2],
        vec![5, 3, 6, 7, 4, 2],
        vec![5, 3, 0, 1, 4, 2],
        vec![5, 4, 1, 0, 2],
        vec![5, 7, 4, 1, 0, 2],
        vec![5, 6, 7, 4, 1, 0, 2],
        vec![5, 3, 6, 7, 4, 1, 0, 2],
        vec![5, 6, 3, 0, 2],
        vec![5, 7, 6, 3, 0, 2],
        vec![5, 4, 7, 6, 3, 0, 2],
        vec![5, 1, 4, 7, 6, 3, 0, 2]
    ]);

    assert_eq!(result, shared::traverse(&graph, 5, 2));
}

#[test]
fn test_traverse_b62() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![5, 1],
        vec![5, 4, 1],
        vec![5, 2, 4, 1],
        vec![5, 2, 0, 1],
        vec![5, 3, 0, 1],
        vec![5, 7, 4, 1],
        vec![5, 6, 7, 4, 1],
        vec![5, 3, 6, 7, 4, 1],
        vec![5, 3, 0, 2, 4, 1],
        vec![5, 4, 2, 0, 1],
        vec![5, 7, 4, 2, 0, 1],
        vec![5, 6, 7, 4, 2, 0, 1],
        vec![5, 3, 6, 7, 4, 2, 0, 1],
        vec![5, 6, 3, 0, 1],
        vec![5, 7, 6, 3, 0, 1],
        vec![5, 4, 7, 6, 3, 0, 1],
        vec![5, 2, 4, 7, 6, 3, 0, 1]
    ]);

    assert_eq!(result, shared::traverse(&graph, 5, 1));
}

#[test]
fn test_traverse_b61() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![5, 1, 0],
        vec![5, 2, 0],
        vec![5, 3, 0],
        vec![5, 4, 1, 0],
        vec![5, 2, 4, 1, 0],
        vec![5, 7, 4, 1, 0],
        vec![5, 6, 7, 4, 1, 0],
        vec![5, 3, 6, 7, 4, 1, 0],
        vec![5, 4, 2, 0],
        vec![5, 1, 4, 2, 0],
        vec![5, 7, 4, 2, 0],
        vec![5, 6, 7, 4, 2, 0],
        vec![5, 3, 6, 7, 4, 2, 0],
        vec![5, 6, 3, 0],
        vec![5, 7, 6, 3, 0],
        vec![5, 4, 7, 6, 3, 0],
        vec![5, 1, 4, 7, 6, 3, 0],
        vec![5, 2, 4, 7, 6, 3, 0]
    ]);

    assert_eq!(result, shared::traverse(&graph, 5, 0));
}

#[test]
fn test_traverse_b78() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![6, 7],
        vec![6, 5, 7],
        vec![6, 3, 5, 7],
        vec![6, 5, 4, 7],
        vec![6, 3, 5, 4, 7],
        vec![6, 5, 1, 4, 7],
        vec![6, 3, 5, 1, 4, 7],
        vec![6, 3, 0, 1, 4, 7],
        vec![6, 5, 2, 4, 7],
        vec![6, 3, 5, 2, 4, 7],
        vec![6, 3, 0, 2, 4, 7],
        vec![6, 3, 0, 1, 5, 7],
        vec![6, 3, 0, 2, 5, 7],
        vec![6, 3, 0, 1, 4, 5, 7],
        vec![6, 3, 0, 2, 4, 5, 7],
        vec![6, 3, 0, 1, 5, 4, 7],
        vec![6, 3, 0, 2, 5, 4, 7],
        vec![6, 3, 0, 2, 5, 1, 4, 7],
        vec![6, 3, 0, 1, 5, 2, 4, 7],
        vec![6, 3, 0, 2, 4, 1, 5, 7],
        vec![6, 3, 0, 1, 4, 2, 5, 7],
        vec![6, 5, 2, 0, 1, 4, 7],
        vec![6, 3, 5, 2, 0, 1, 4, 7],
        vec![6, 5, 3, 0, 1, 4, 7],
        vec![6, 5, 1, 0, 2, 4, 7],
        vec![6, 3, 5, 1, 0, 2, 4, 7],
        vec![6, 5, 3, 0, 2, 4, 7]
    ]);

    assert_eq!(result, shared::traverse(&graph, 6, 7));
}

#[test]
fn test_traverse_b76() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![6, 5],
        vec![6, 3, 5],
        vec![6, 7, 5],
        vec![6, 3, 0, 1, 5],
        vec![6, 3, 0, 2, 5],
        vec![6, 7, 4, 5],
        vec![6, 3, 0, 1, 4, 5],
        vec![6, 3, 0, 2, 4, 5],
        vec![6, 3, 0, 1, 4, 7, 5],
        vec![6, 3, 0, 2, 4, 7, 5],
        vec![6, 7, 4, 1, 5],
        vec![6, 3, 0, 2, 4, 1, 5],
        vec![6, 7, 4, 2, 5],
        vec![6, 3, 0, 1, 4, 2, 5],
        vec![6, 7, 4, 2, 0, 1, 5],
        vec![6, 7, 4, 1, 0, 2, 5],
        vec![6, 7, 4, 1, 0, 3, 5],
        vec![6, 7, 4, 2, 0, 3, 5]
    ]);

    assert_eq!(result, shared::traverse(&graph, 6, 5));
}

#[test]
fn test_traverse_b75() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![6, 5, 4],
        vec![6, 3, 5, 4],
        vec![6, 7, 4],
        vec![6, 5, 7, 4],
        vec![6, 3, 5, 7, 4],
        vec![6, 7, 5, 4],
        vec![6, 5, 1, 4],
        vec![6, 3, 5, 1, 4],
        vec![6, 3, 0, 1, 4],
        vec![6, 7, 5, 1, 4],
        vec![6, 5, 2, 4],
        vec![6, 3, 5, 2, 4],
        vec![6, 3, 0, 2, 4],
        vec![6, 7, 5, 2, 4],
        vec![6, 3, 0, 1, 5, 4],
        vec![6, 3, 0, 2, 5, 4],
        vec![6, 3, 0, 2, 5, 1, 4],
        vec![6, 3, 0, 1, 5, 2, 4],
        vec![6, 5, 2, 0, 1, 4],
        vec![6, 3, 5, 2, 0, 1, 4],
        vec![6, 7, 5, 2, 0, 1, 4],
        vec![6, 5, 3, 0, 1, 4],
        vec![6, 7, 5, 3, 0, 1, 4],
        vec![6, 5, 1, 0, 2, 4],
        vec![6, 3, 5, 1, 0, 2, 4],
        vec![6, 7, 5, 1, 0, 2, 4],
        vec![6, 5, 3, 0, 2, 4],
        vec![6, 7, 5, 3, 0, 2, 4],
        vec![6, 3, 0, 1, 5, 7, 4],
        vec![6, 3, 0, 2, 5, 7, 4]
    ]);

    assert_eq!(result, shared::traverse(&graph, 6, 4));
}

#[test]
fn test_traverse_b74() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![6, 3],
        vec![6, 5, 3],
        vec![6, 7, 5, 3],
        vec![6, 7, 4, 5, 3],
        vec![6, 5, 1, 0, 3],
        vec![6, 7, 5, 1, 0, 3],
        vec![6, 5, 2, 0, 3],
        vec![6, 7, 5, 2, 0, 3],
        vec![6, 5, 4, 1, 0, 3],
        vec![6, 7, 4, 1, 0, 3],
        vec![6, 5, 7, 4, 1, 0, 3],
        vec![6, 7, 5, 4, 1, 0, 3],
        vec![6, 5, 2, 4, 1, 0, 3],
        vec![6, 7, 5, 2, 4, 1, 0, 3],
        vec![6, 7, 4, 5, 1, 0, 3],
        vec![6, 5, 4, 2, 0, 3],
        vec![6, 7, 4, 2, 0, 3],
        vec![6, 5, 7, 4, 2, 0, 3],
        vec![6, 7, 5, 4, 2, 0, 3],
        vec![6, 5, 1, 4, 2, 0, 3],
        vec![6, 7, 5, 1, 4, 2, 0, 3],
        vec![6, 7, 4, 5, 2, 0, 3],
        vec![6, 7, 4, 1, 5, 3],
        vec![6, 7, 4, 2, 5, 3],
        vec![6, 7, 4, 2, 0, 1, 5, 3],
        vec![6, 7, 4, 1, 0, 2, 5, 3],
        vec![6, 7, 4, 2, 5, 1, 0, 3],
        vec![6, 7, 4, 1, 5, 2, 0, 3]
    ]);

    assert_eq!(result, shared::traverse(&graph, 6, 3));
}

#[test]
fn test_traverse_b73() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![6, 5, 2],
        vec![6, 3, 5, 2],
        vec![6, 3, 0, 2],
        vec![6, 7, 5, 2],
        vec![6, 5, 4, 2],
        vec![6, 3, 5, 4, 2],
        vec![6, 7, 4, 2],
        vec![6, 5, 7, 4, 2],
        vec![6, 3, 5, 7, 4, 2],
        vec![6, 7, 5, 4, 2],
        vec![6, 5, 1, 4, 2],
        vec![6, 3, 5, 1, 4, 2],
        vec![6, 3, 0, 1, 4, 2],
        vec![6, 7, 5, 1, 4, 2],
        vec![6, 3, 0, 1, 5, 2],
        vec![6, 7, 4, 5, 2],
        vec![6, 3, 0, 1, 4, 5, 2],
        vec![6, 3, 0, 1, 4, 7, 5, 2],
        vec![6, 5, 1, 0, 2],
        vec![6, 3, 5, 1, 0, 2],
        vec![6, 7, 5, 1, 0, 2],
        vec![6, 5, 3, 0, 2],
        vec![6, 7, 5, 3, 0, 2],
        vec![6, 5, 4, 1, 0, 2],
        vec![6, 3, 5, 4, 1, 0, 2],
        vec![6, 7, 4, 1, 0, 2],
        vec![6, 5, 7, 4, 1, 0, 2],
        vec![6, 3, 5, 7, 4, 1, 0, 2],
        vec![6, 7, 5, 4, 1, 0, 2],
        vec![6, 7, 4, 5, 1, 0, 2],
        vec![6, 7, 4, 5, 3, 0, 2],
        vec![6, 3, 0, 1, 5, 4, 2],
        vec![6, 7, 4, 1, 5, 2],
        vec![6, 5, 3, 0, 1, 4, 2],
        vec![6, 7, 5, 3, 0, 1, 4, 2],
        vec![6, 3, 0, 1, 5, 7, 4, 2]
    ]);

    assert_eq!(result, shared::traverse(&graph, 6, 2));
}

#[test]
fn test_traverse_b72() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![6, 5, 1],
        vec![6, 3, 5, 1],
        vec![6, 3, 0, 1],
        vec![6, 7, 5, 1],
        vec![6, 5, 4, 1],
        vec![6, 3, 5, 4, 1],
        vec![6, 7, 4, 1],
        vec![6, 5, 7, 4, 1],
        vec![6, 3, 5, 7, 4, 1],
        vec![6, 7, 5, 4, 1],
        vec![6, 5, 2, 4, 1],
        vec![6, 3, 5, 2, 4, 1],
        vec![6, 3, 0, 2, 4, 1],
        vec![6, 7, 5, 2, 4, 1],
        vec![6, 3, 0, 2, 5, 1],
        vec![6, 7, 4, 5, 1],
        vec![6, 3, 0, 2, 4, 5, 1],
        vec![6, 3, 0, 2, 4, 7, 5, 1],
        vec![6, 5, 2, 0, 1],
        vec![6, 3, 5, 2, 0, 1],
        vec![6, 7, 5, 2, 0, 1],
        vec![6, 5, 3, 0, 1],
        vec![6, 7, 5, 3, 0, 1],
        vec![6, 5, 4, 2, 0, 1],
        vec![6, 3, 5, 4, 2, 0, 1],
        vec![6, 7, 4, 2, 0, 1],
        vec![6, 5, 7, 4, 2, 0, 1],
        vec![6, 3, 5, 7, 4, 2, 0, 1],
        vec![6, 7, 5, 4, 2, 0, 1],
        vec![6, 7, 4, 5, 2, 0, 1],
        vec![6, 7, 4, 5, 3, 0, 1],
        vec![6, 3, 0, 2, 5, 4, 1],
        vec![6, 7, 4, 2, 5, 1],
        vec![6, 5, 3, 0, 2, 4, 1],
        vec![6, 7, 5, 3, 0, 2, 4, 1],
        vec![6, 3, 0, 2, 5, 7, 4, 1]
    ]);

    assert_eq!(result, shared::traverse(&graph, 6, 1));
}

#[test]
fn test_traverse_b71() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![6, 3, 0],
        vec![6, 5, 1, 0],
        vec![6, 3, 5, 1, 0],
        vec![6, 7, 5, 1, 0],
        vec![6, 5, 2, 0],
        vec![6, 3, 5, 2, 0],
        vec![6, 7, 5, 2, 0],
        vec![6, 5, 3, 0],
        vec![6, 7, 5, 3, 0],
        vec![6, 5, 4, 1, 0],
        vec![6, 3, 5, 4, 1, 0],
        vec![6, 7, 4, 1, 0],
        vec![6, 5, 7, 4, 1, 0],
        vec![6, 3, 5, 7, 4, 1, 0],
        vec![6, 7, 5, 4, 1, 0],
        vec![6, 5, 2, 4, 1, 0],
        vec![6, 3, 5, 2, 4, 1, 0],
        vec![6, 7, 5, 2, 4, 1, 0],
        vec![6, 7, 4, 5, 1, 0],
        vec![6, 5, 4, 2, 0],
        vec![6, 3, 5, 4, 2, 0],
        vec![6, 7, 4, 2, 0],
        vec![6, 5, 7, 4, 2, 0],
        vec![6, 3, 5, 7, 4, 2, 0],
        vec![6, 7, 5, 4, 2, 0],
        vec![6, 5, 1, 4, 2, 0],
        vec![6, 3, 5, 1, 4, 2, 0],
        vec![6, 7, 5, 1, 4, 2, 0],
        vec![6, 7, 4, 5, 2, 0],
        vec![6, 7, 4, 5, 3, 0],
        vec![6, 7, 4, 2, 5, 1, 0],
        vec![6, 7, 4, 1, 5, 2, 0],
        vec![6, 7, 4, 1, 5, 3, 0],
        vec![6, 7, 4, 2, 5, 3, 0]
    ]);

    assert_eq!(result, shared::traverse(&graph, 6, 0));
}

#[test]
fn test_traverse_b87() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![7, 6],
        vec![7, 5, 6],
        vec![7, 4, 5, 6],
        vec![7, 4, 1, 5, 6],
        vec![7, 4, 2, 5, 6],
        vec![7, 5, 3, 6],
        vec![7, 4, 5, 3, 6],
        vec![7, 4, 1, 5, 3, 6],
        vec![7, 4, 2, 5, 3, 6],
        vec![7, 4, 1, 0, 3, 6],
        vec![7, 5, 1, 0, 3, 6],
        vec![7, 4, 5, 1, 0, 3, 6],
        vec![7, 4, 2, 0, 3, 6],
        vec![7, 5, 2, 0, 3, 6],
        vec![7, 4, 5, 2, 0, 3, 6],
        vec![7, 4, 2, 5, 1, 0, 3, 6],
        vec![7, 4, 1, 5, 2, 0, 3, 6],
        vec![7, 4, 2, 0, 1, 5, 6],
        vec![7, 4, 1, 0, 2, 5, 6],
        vec![7, 4, 1, 0, 3, 5, 6],
        vec![7, 4, 2, 0, 3, 5, 6]
    ]);

    assert_eq!(result, shared::traverse(&graph, 7, 6));
}

#[test]
fn test_traverse_b86() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![7, 5],
        vec![7, 4, 5],
        vec![7, 6, 5],
        vec![7, 4, 1, 5],
        vec![7, 4, 2, 5],
        vec![7, 6, 3, 5],
        vec![7, 4, 2, 0, 1, 5],
        vec![7, 6, 3, 0, 1, 5],
        vec![7, 4, 1, 0, 2, 5],
        vec![7, 6, 3, 0, 2, 5],
        vec![7, 4, 1, 0, 3, 5],
        vec![7, 4, 2, 0, 3, 5],
        vec![7, 6, 3, 0, 1, 4, 5],
        vec![7, 6, 3, 0, 2, 4, 5],
        vec![7, 4, 1, 0, 3, 6, 5],
        vec![7, 4, 2, 0, 3, 6, 5]
    ]);

    assert_eq!(result, shared::traverse(&graph, 7, 5));
}

#[test]
fn test_traverse_b85() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![7, 4],
        vec![7, 5, 4],
        vec![7, 5, 1, 4],
        vec![7, 5, 2, 4],
        vec![7, 6, 5, 4],
        vec![7, 6, 5, 1, 4],
        vec![7, 6, 5, 2, 4],
        vec![7, 6, 3, 5, 4],
        vec![7, 6, 3, 5, 1, 4],
        vec![7, 5, 2, 0, 1, 4],
        vec![7, 6, 5, 2, 0, 1, 4],
        vec![7, 5, 3, 0, 1, 4],
        vec![7, 6, 3, 0, 1, 4],
        vec![7, 5, 6, 3, 0, 1, 4],
        vec![7, 6, 5, 3, 0, 1, 4],
        vec![7, 6, 3, 5, 2, 4],
        vec![7, 5, 1, 0, 2, 4],
        vec![7, 6, 5, 1, 0, 2, 4],
        vec![7, 5, 3, 0, 2, 4],
        vec![7, 6, 3, 0, 2, 4],
        vec![7, 5, 6, 3, 0, 2, 4],
        vec![7, 6, 5, 3, 0, 2, 4],
        vec![7, 6, 3, 0, 1, 5, 4],
        vec![7, 6, 3, 0, 2, 5, 4]
    ]);

    assert_eq!(result, shared::traverse(&graph, 7, 4));
}

#[test]
fn test_traverse_b84() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![7, 5, 3],
        vec![7, 4, 5, 3],
        vec![7, 6, 3],
        vec![7, 5, 6, 3],
        vec![7, 4, 5, 6, 3],
        vec![7, 6, 5, 3],
        vec![7, 4, 1, 5, 3],
        vec![7, 4, 2, 5, 3],
        vec![7, 4, 1, 5, 6, 3],
        vec![7, 4, 2, 5, 6, 3],
        vec![7, 4, 1, 0, 3],
        vec![7, 5, 1, 0, 3],
        vec![7, 4, 5, 1, 0, 3],
        vec![7, 4, 2, 0, 3],
        vec![7, 5, 2, 0, 3],
        vec![7, 4, 5, 2, 0, 3],
        vec![7, 6, 5, 1, 0, 3],
        vec![7, 4, 2, 5, 1, 0, 3],
        vec![7, 6, 5, 2, 0, 3],
        vec![7, 4, 1, 5, 2, 0, 3],
        vec![7, 5, 4, 1, 0, 3],
        vec![7, 5, 2, 4, 1, 0, 3],
        vec![7, 6, 5, 4, 1, 0, 3],
        vec![7, 6, 5, 2, 4, 1, 0, 3],
        vec![7, 4, 2, 0, 1, 5, 3],
        vec![7, 5, 4, 2, 0, 3],
        vec![7, 5, 1, 4, 2, 0, 3],
        vec![7, 6, 5, 4, 2, 0, 3],
        vec![7, 6, 5, 1, 4, 2, 0, 3],
        vec![7, 4, 1, 0, 2, 5, 3]
    ]);

    assert_eq!(result, shared::traverse(&graph, 7, 3));
}

#[test]
fn test_traverse_b83() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![7, 4, 2],
        vec![7, 5, 2],
        vec![7, 4, 5, 2],
        vec![7, 6, 5, 2],
        vec![7, 4, 1, 5, 2],
        vec![7, 5, 4, 2],
        vec![7, 5, 1, 4, 2],
        vec![7, 6, 5, 4, 2],
        vec![7, 6, 5, 1, 4, 2],
        vec![7, 6, 3, 5, 2],
        vec![7, 4, 1, 0, 2],
        vec![7, 5, 1, 0, 2],
        vec![7, 4, 5, 1, 0, 2],
        vec![7, 6, 5, 1, 0, 2],
        vec![7, 5, 3, 0, 2],
        vec![7, 4, 5, 3, 0, 2],
        vec![7, 6, 3, 0, 2],
        vec![7, 5, 6, 3, 0, 2],
        vec![7, 4, 5, 6, 3, 0, 2],
        vec![7, 6, 5, 3, 0, 2],
        vec![7, 4, 1, 5, 3, 0, 2],
        vec![7, 4, 1, 5, 6, 3, 0, 2],
        vec![7, 6, 3, 5, 4, 2],
        vec![7, 5, 4, 1, 0, 2],
        vec![7, 6, 5, 4, 1, 0, 2],
        vec![7, 6, 3, 5, 1, 0, 2],
        vec![7, 6, 3, 5, 1, 4, 2],
        vec![7, 5, 3, 0, 1, 4, 2],
        vec![7, 6, 3, 0, 1, 4, 2],
        vec![7, 6, 3, 0, 1, 5, 2],
        vec![7, 5, 6, 3, 0, 1, 4, 2],
        vec![7, 6, 5, 3, 0, 1, 4, 2],
        vec![7, 6, 3, 5, 4, 1, 0, 2],
        vec![7, 4, 1, 0, 3, 5, 2]
    ]);

    assert_eq!(result, shared::traverse(&graph, 7, 2));
}

#[test]
fn test_traverse_b82() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![7, 4, 1],
        vec![7, 5, 1],
        vec![7, 4, 5, 1],
        vec![7, 6, 5, 1],
        vec![7, 4, 2, 5, 1],
        vec![7, 5, 4, 1],
        vec![7, 5, 2, 4, 1],
        vec![7, 6, 5, 4, 1],
        vec![7, 6, 5, 2, 4, 1],
        vec![7, 6, 3, 5, 1],
        vec![7, 4, 2, 0, 1],
        vec![7, 5, 2, 0, 1],
        vec![7, 4, 5, 2, 0, 1],
        vec![7, 6, 5, 2, 0, 1],
        vec![7, 5, 3, 0, 1],
        vec![7, 4, 5, 3, 0, 1],
        vec![7, 6, 3, 0, 1],
        vec![7, 5, 6, 3, 0, 1],
        vec![7, 4, 5, 6, 3, 0, 1],
        vec![7, 6, 5, 3, 0, 1],
        vec![7, 4, 2, 5, 3, 0, 1],
        vec![7, 4, 2, 5, 6, 3, 0, 1],
        vec![7, 6, 3, 5, 4, 1],
        vec![7, 5, 4, 2, 0, 1],
        vec![7, 6, 5, 4, 2, 0, 1],
        vec![7, 6, 3, 5, 2, 0, 1],
        vec![7, 6, 3, 5, 2, 4, 1],
        vec![7, 5, 3, 0, 2, 4, 1],
        vec![7, 6, 3, 0, 2, 4, 1],
        vec![7, 6, 3, 0, 2, 5, 1],
        vec![7, 5, 6, 3, 0, 2, 4, 1],
        vec![7, 6, 5, 3, 0, 2, 4, 1],
        vec![7, 6, 3, 5, 4, 2, 0, 1],
        vec![7, 4, 2, 0, 3, 5, 1]
    ]);

    assert_eq!(result, shared::traverse(&graph, 7, 1));
}

#[test]
fn test_traverse_b81() {
    let graph: VecDeque<Vec<usize>> = setup_graph_b();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![7, 4, 1, 0],
        vec![7, 5, 1, 0],
        vec![7, 4, 5, 1, 0],
        vec![7, 4, 2, 0],
        vec![7, 5, 2, 0],
        vec![7, 4, 5, 2, 0],
        vec![7, 6, 5, 1, 0],
        vec![7, 4, 2, 5, 1, 0],
        vec![7, 6, 5, 2, 0],
        vec![7, 4, 1, 5, 2, 0],
        vec![7, 5, 3, 0],
        vec![7, 4, 5, 3, 0],
        vec![7, 6, 3, 0],
        vec![7, 5, 6, 3, 0],
        vec![7, 4, 5, 6, 3, 0],
        vec![7, 6, 5, 3, 0],
        vec![7, 4, 1, 5, 3, 0],
        vec![7, 4, 2, 5, 3, 0],
        vec![7, 4, 1, 5, 6, 3, 0],
        vec![7, 4, 2, 5, 6, 3, 0],
        vec![7, 5, 4, 1, 0],
        vec![7, 5, 2, 4, 1, 0],
        vec![7, 6, 5, 4, 1, 0],
        vec![7, 6, 5, 2, 4, 1, 0],
        vec![7, 6, 3, 5, 1, 0],
        vec![7, 6, 3, 5, 4, 1, 0],
        vec![7, 5, 4, 2, 0],
        vec![7, 5, 1, 4, 2, 0],
        vec![7, 6, 5, 4, 2, 0],
        vec![7, 6, 5, 1, 4, 2, 0],
        vec![7, 6, 3, 5, 2, 0],
        vec![7, 6, 3, 5, 4, 2, 0]
    ]);

    assert_eq!(result, shared::traverse(&graph, 7, 0));
}

#[test]
fn test_traverse_c15() {
    let graph: VecDeque<Vec<usize>> = setup_graph_c();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![0, 4],
        vec![0, 1, 4],
        vec![0, 2, 4],
        vec![0, 1, 2, 4],
        vec![0, 3, 4],
        vec![0, 1, 3, 4],
        vec![0, 2, 3, 4],
        vec![0, 1, 2, 3, 4],
        vec![0, 3, 2, 4],
        vec![0, 1, 3, 2, 4],
        vec![0, 2, 1, 4],
        vec![0, 3, 1, 4],
        vec![0, 2, 3, 1, 4],
        vec![0, 3, 2, 1, 4],
        vec![0, 2, 1, 3, 4],
        vec![0, 3, 1, 2, 4]
    ]);

    assert_eq!(result, shared::traverse(&graph, 0, 4));
}

#[test]
fn test_traverse_c14() {
    let graph: VecDeque<Vec<usize>> = setup_graph_c();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![0, 3],
        vec![0, 1, 3],
        vec![0, 2, 3],
        vec![0, 1, 2, 3],
        vec![0, 4, 3],
        vec![0, 1, 4, 3],
        vec![0, 2, 4, 3],
        vec![0, 1, 2, 4, 3],
        vec![0, 4, 2, 3],
        vec![0, 1, 4, 2, 3],
        vec![0, 2, 1, 3],
        vec![0, 4, 1, 3],
        vec![0, 2, 4, 1, 3],
        vec![0, 4, 2, 1, 3],
        vec![0, 2, 1, 4, 3],
        vec![0, 4, 1, 2, 3]
    ]);

    assert_eq!(result, shared::traverse(&graph, 0, 3));
}

#[test]
fn test_traverse_c13() {
    let graph: VecDeque<Vec<usize>> = setup_graph_c();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![0, 2],
        vec![0, 1, 2],
        vec![0, 3, 2],
        vec![0, 1, 3, 2],
        vec![0, 4, 2],
        vec![0, 1, 4, 2],
        vec![0, 3, 4, 2],
        vec![0, 1, 3, 4, 2],
        vec![0, 4, 3, 2],
        vec![0, 1, 4, 3, 2],
        vec![0, 3, 1, 2],
        vec![0, 4, 1, 2],
        vec![0, 3, 4, 1, 2],
        vec![0, 4, 3, 1, 2],
        vec![0, 4, 1, 3, 2],
        vec![0, 3, 1, 4, 2]
    ]);

    assert_eq!(result, shared::traverse(&graph, 0, 2));
}

#[test]
fn test_traverse_c12() {
    let graph: VecDeque<Vec<usize>> = setup_graph_c();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![0, 1],
        vec![0, 2, 1],
        vec![0, 3, 1],
        vec![0, 2, 3, 1],
        vec![0, 4, 1],
        vec![0, 2, 4, 1],
        vec![0, 3, 4, 1],
        vec![0, 2, 3, 4, 1],
        vec![0, 3, 2, 1],
        vec![0, 4, 2, 1],
        vec![0, 3, 4, 2, 1],
        vec![0, 4, 3, 1],
        vec![0, 2, 4, 3, 1],
        vec![0, 4, 2, 3, 1],
        vec![0, 3, 2, 4, 1],
        vec![0, 4, 3, 2, 1]
    ]);

    assert_eq!(result, shared::traverse(&graph, 0, 1));
}

#[test]
fn test_traverse_c25() {
    let graph: VecDeque<Vec<usize>> = setup_graph_c();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![1, 4],
        vec![1, 0, 4],
        vec![1, 2, 4],
        vec![1, 0, 2, 4],
        vec![1, 3, 4],
        vec![1, 0, 3, 4],
        vec![1, 2, 3, 4],
        vec![1, 0, 2, 3, 4],
        vec![1, 3, 2, 4],
        vec![1, 0, 3, 2, 4],
        vec![1, 2, 0, 4],
        vec![1, 3, 0, 4],
        vec![1, 2, 3, 0, 4],
        vec![1, 3, 2, 0, 4],
        vec![1, 2, 0, 3, 4],
        vec![1, 3, 0, 2, 4]
    ]);

    assert_eq!(result, shared::traverse(&graph, 1, 4));
}

#[test]
fn test_traverse_c24() {
    let graph: VecDeque<Vec<usize>> = setup_graph_c();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![1, 3],
        vec![1, 0, 3],
        vec![1, 2, 3],
        vec![1, 0, 2, 3],
        vec![1, 4, 3],
        vec![1, 0, 4, 3],
        vec![1, 2, 4, 3],
        vec![1, 0, 2, 4, 3],
        vec![1, 4, 2, 3],
        vec![1, 0, 4, 2, 3],
        vec![1, 2, 0, 3],
        vec![1, 4, 0, 3],
        vec![1, 2, 4, 0, 3],
        vec![1, 4, 2, 0, 3],
        vec![1, 2, 0, 4, 3],
        vec![1, 4, 0, 2, 3]
    ]);

    assert_eq!(result, shared::traverse(&graph, 1, 3));
}

#[test]
fn test_traverse_c23() {
    let graph: VecDeque<Vec<usize>> = setup_graph_c();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![1, 2],
        vec![1, 0, 2],
        vec![1, 3, 2],
        vec![1, 0, 3, 2],
        vec![1, 4, 2],
        vec![1, 0, 4, 2],
        vec![1, 3, 4, 2],
        vec![1, 0, 3, 4, 2],
        vec![1, 4, 3, 2],
        vec![1, 0, 4, 3, 2],
        vec![1, 3, 0, 2],
        vec![1, 4, 0, 2],
        vec![1, 3, 4, 0, 2],
        vec![1, 4, 3, 0, 2],
        vec![1, 4, 0, 3, 2],
        vec![1, 3, 0, 4, 2]
    ]);

    assert_eq!(result, shared::traverse(&graph, 1, 2));
}

#[test]
fn test_traverse_c21() {
    let graph: VecDeque<Vec<usize>> = setup_graph_c();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![1, 0],
        vec![1, 2, 0],
        vec![1, 3, 0],
        vec![1, 2, 3, 0],
        vec![1, 4, 0],
        vec![1, 2, 4, 0],
        vec![1, 3, 4, 0],
        vec![1, 2, 3, 4, 0],
        vec![1, 3, 2, 0],
        vec![1, 4, 2, 0],
        vec![1, 3, 4, 2, 0],
        vec![1, 4, 3, 0],
        vec![1, 2, 4, 3, 0],
        vec![1, 4, 2, 3, 0],
        vec![1, 3, 2, 4, 0],
        vec![1, 4, 3, 2, 0]
    ]);

    assert_eq!(result, shared::traverse(&graph, 1, 0));
}

#[test]
fn test_traverse_c35() {
    let graph: VecDeque<Vec<usize>> = setup_graph_c();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![2, 4],
        vec![2, 0, 4],
        vec![2, 1, 4],
        vec![2, 0, 1, 4],
        vec![2, 3, 4],
        vec![2, 0, 3, 4],
        vec![2, 1, 3, 4],
        vec![2, 0, 1, 3, 4],
        vec![2, 3, 1, 4],
        vec![2, 0, 3, 1, 4],
        vec![2, 1, 0, 4],
        vec![2, 3, 0, 4],
        vec![2, 1, 3, 0, 4],
        vec![2, 3, 1, 0, 4],
        vec![2, 1, 0, 3, 4],
        vec![2, 3, 0, 1, 4]
    ]);

    assert_eq!(result, shared::traverse(&graph, 2, 4));
}

#[test]
fn test_traverse_c34() {
    let graph: VecDeque<Vec<usize>> = setup_graph_c();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![2, 3],
        vec![2, 0, 3],
        vec![2, 1, 3],
        vec![2, 0, 1, 3],
        vec![2, 4, 3],
        vec![2, 0, 4, 3],
        vec![2, 1, 4, 3],
        vec![2, 0, 1, 4, 3],
        vec![2, 4, 1, 3],
        vec![2, 0, 4, 1, 3],
        vec![2, 1, 0, 3],
        vec![2, 4, 0, 3],
        vec![2, 1, 4, 0, 3],
        vec![2, 4, 1, 0, 3],
        vec![2, 1, 0, 4, 3],
        vec![2, 4, 0, 1, 3]
    ]);

    assert_eq!(result, shared::traverse(&graph, 2, 3));
}

#[test]
fn test_traverse_c32() {
    let graph: VecDeque<Vec<usize>> = setup_graph_c();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![2, 1],
        vec![2, 0, 1],
        vec![2, 3, 1],
        vec![2, 0, 3, 1],
        vec![2, 4, 1],
        vec![2, 0, 4, 1],
        vec![2, 3, 4, 1],
        vec![2, 0, 3, 4, 1],
        vec![2, 4, 3, 1],
        vec![2, 0, 4, 3, 1],
        vec![2, 3, 0, 1],
        vec![2, 4, 0, 1],
        vec![2, 3, 4, 0, 1],
        vec![2, 4, 3, 0, 1],
        vec![2, 4, 0, 3, 1],
        vec![2, 3, 0, 4, 1]
    ]);

    assert_eq!(result, shared::traverse(&graph, 2, 1));
}

#[test]
fn test_traverse_c31() {
    let graph: VecDeque<Vec<usize>> = setup_graph_c();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![2, 0],
        vec![2, 1, 0],
        vec![2, 3, 0],
        vec![2, 1, 3, 0],
        vec![2, 4, 0],
        vec![2, 1, 4, 0],
        vec![2, 3, 4, 0],
        vec![2, 1, 3, 4, 0],
        vec![2, 3, 1, 0],
        vec![2, 4, 1, 0],
        vec![2, 3, 4, 1, 0],
        vec![2, 4, 3, 0],
        vec![2, 1, 4, 3, 0],
        vec![2, 4, 1, 3, 0],
        vec![2, 3, 1, 4, 0],
        vec![2, 4, 3, 1, 0]
    ]);

    assert_eq!(result, shared::traverse(&graph, 2, 0));
}

#[test]
fn test_traverse_c45() {
    let graph: VecDeque<Vec<usize>> = setup_graph_c();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![3, 4],
        vec![3, 0, 4],
        vec![3, 1, 4],
        vec![3, 0, 1, 4],
        vec![3, 2, 4],
        vec![3, 0, 2, 4],
        vec![3, 1, 2, 4],
        vec![3, 0, 1, 2, 4],
        vec![3, 2, 1, 4],
        vec![3, 0, 2, 1, 4],
        vec![3, 1, 0, 4],
        vec![3, 2, 0, 4],
        vec![3, 1, 2, 0, 4],
        vec![3, 2, 1, 0, 4],
        vec![3, 1, 0, 2, 4],
        vec![3, 2, 0, 1, 4]
    ]);

    assert_eq!(result, shared::traverse(&graph, 3, 4));
}

#[test]
fn test_traverse_c43() {
    let graph: VecDeque<Vec<usize>> = setup_graph_c();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![3, 2],
        vec![3, 0, 2],
        vec![3, 1, 2],
        vec![3, 0, 1, 2],
        vec![3, 4, 2],
        vec![3, 0, 4, 2],
        vec![3, 1, 4, 2],
        vec![3, 0, 1, 4, 2],
        vec![3, 4, 1, 2],
        vec![3, 0, 4, 1, 2],
        vec![3, 1, 0, 2],
        vec![3, 4, 0, 2],
        vec![3, 1, 4, 0, 2],
        vec![3, 4, 1, 0, 2],
        vec![3, 1, 0, 4, 2],
        vec![3, 4, 0, 1, 2]
    ]);

    assert_eq!(result, shared::traverse(&graph, 3, 2));
}

#[test]
fn test_traverse_c42() {
    let graph: VecDeque<Vec<usize>> = setup_graph_c();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![3, 1],
        vec![3, 0, 1],
        vec![3, 2, 1],
        vec![3, 0, 2, 1],
        vec![3, 4, 1],
        vec![3, 0, 4, 1],
        vec![3, 2, 4, 1],
        vec![3, 0, 2, 4, 1],
        vec![3, 4, 2, 1],
        vec![3, 0, 4, 2, 1],
        vec![3, 2, 0, 1],
        vec![3, 4, 0, 1],
        vec![3, 2, 4, 0, 1],
        vec![3, 4, 2, 0, 1],
        vec![3, 4, 0, 2, 1],
        vec![3, 2, 0, 4, 1]
    ]);

    assert_eq!(result, shared::traverse(&graph, 3, 1));
}

#[test]
fn test_traverse_c41() {
    let graph: VecDeque<Vec<usize>> = setup_graph_c();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![3, 0],
        vec![3, 1, 0],
        vec![3, 2, 0],
        vec![3, 1, 2, 0],
        vec![3, 4, 0],
        vec![3, 1, 4, 0],
        vec![3, 2, 4, 0],
        vec![3, 1, 2, 4, 0],
        vec![3, 2, 1, 0],
        vec![3, 4, 1, 0],
        vec![3, 2, 4, 1, 0],
        vec![3, 4, 2, 0],
        vec![3, 1, 4, 2, 0],
        vec![3, 4, 1, 2, 0],
        vec![3, 2, 1, 4, 0],
        vec![3, 4, 2, 1, 0]
    ]);

    assert_eq!(result, shared::traverse(&graph, 3, 0));
}

#[test]
fn test_traverse_c54() {
    let graph: VecDeque<Vec<usize>> = setup_graph_c();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![4, 3],
        vec![4, 0, 3],
        vec![4, 1, 3],
        vec![4, 0, 1, 3],
        vec![4, 2, 3],
        vec![4, 0, 2, 3],
        vec![4, 1, 2, 3],
        vec![4, 0, 1, 2, 3],
        vec![4, 2, 1, 3],
        vec![4, 0, 2, 1, 3],
        vec![4, 1, 0, 3],
        vec![4, 2, 0, 3],
        vec![4, 1, 2, 0, 3],
        vec![4, 2, 1, 0, 3],
        vec![4, 1, 0, 2, 3],
        vec![4, 2, 0, 1, 3]
    ]);

    assert_eq!(result, shared::traverse(&graph, 4, 3));
}

#[test]
fn test_traverse_c53() {
    let graph: VecDeque<Vec<usize>> = setup_graph_c();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![4, 2],
        vec![4, 0, 2],
        vec![4, 1, 2],
        vec![4, 0, 1, 2],
        vec![4, 3, 2],
        vec![4, 0, 3, 2],
        vec![4, 1, 3, 2],
        vec![4, 0, 1, 3, 2],
        vec![4, 3, 1, 2],
        vec![4, 0, 3, 1, 2],
        vec![4, 1, 0, 2],
        vec![4, 3, 0, 2],
        vec![4, 1, 3, 0, 2],
        vec![4, 3, 1, 0, 2],
        vec![4, 1, 0, 3, 2],
        vec![4, 3, 0, 1, 2]
    ]);

    assert_eq!(result, shared::traverse(&graph, 4, 2));
}

#[test]
fn test_traverse_c52() {
    let graph: VecDeque<Vec<usize>> = setup_graph_c();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![4, 1],
        vec![4, 0, 1],
        vec![4, 2, 1],
        vec![4, 0, 2, 1],
        vec![4, 3, 1],
        vec![4, 0, 3, 1],
        vec![4, 2, 3, 1],
        vec![4, 0, 2, 3, 1],
        vec![4, 3, 2, 1],
        vec![4, 0, 3, 2, 1],
        vec![4, 2, 0, 1],
        vec![4, 3, 0, 1],
        vec![4, 2, 3, 0, 1],
        vec![4, 3, 2, 0, 1],
        vec![4, 3, 0, 2, 1],
        vec![4, 2, 0, 3, 1]
    ]);

    assert_eq!(result, shared::traverse(&graph, 4, 1));
}

#[test]
fn test_traverse_c51() {
    let graph: VecDeque<Vec<usize>> = setup_graph_c();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![4, 0],
        vec![4, 1, 0],
        vec![4, 2, 0],
        vec![4, 1, 2, 0],
        vec![4, 3, 0],
        vec![4, 1, 3, 0],
        vec![4, 2, 3, 0],
        vec![4, 1, 2, 3, 0],
        vec![4, 2, 1, 0],
        vec![4, 3, 1, 0],
        vec![4, 2, 3, 1, 0],
        vec![4, 3, 2, 0],
        vec![4, 1, 3, 2, 0],
        vec![4, 3, 1, 2, 0],
        vec![4, 2, 1, 3, 0],
        vec![4, 3, 2, 1, 0]
    ]);

    assert_eq!(result, shared::traverse(&graph, 4, 0));
}

#[test]
fn test_traverse_d112() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![0, 3, 6, 11]
    ]);

    assert_eq!(result, shared::traverse(&graph, 0, 11));
}

#[test]
fn test_traverse_d111() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![0, 3, 6, 10]
    ]);

    assert_eq!(result, shared::traverse(&graph, 0, 10));
}

#[test]
fn test_traverse_d110() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![0, 1, 4, 9]
    ]);

    assert_eq!(result, shared::traverse(&graph, 0, 9));
}

#[test]
fn test_traverse_d19() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![0, 1, 4, 8]
    ]);

    assert_eq!(result, shared::traverse(&graph, 0, 8));
}

#[test]
fn test_traverse_d18() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![0, 3, 7]
    ]);

    assert_eq!(result, shared::traverse(&graph, 0, 7));
}

#[test]
fn test_traverse_d17() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![0, 3, 6]
    ]);

    assert_eq!(result, shared::traverse(&graph, 0, 6));
}

#[test]
fn test_traverse_d16() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![0, 1, 5]
    ]);

    assert_eq!(result, shared::traverse(&graph, 0, 5));
}

#[test]
fn test_traverse_d15() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![0, 1, 4]
    ]);

    assert_eq!(result, shared::traverse(&graph, 0, 4));
}

#[test]
fn test_traverse_d14() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![0, 3]
    ]);

    assert_eq!(result, shared::traverse(&graph, 0, 3));
}

#[test]
fn test_traverse_d13() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![0, 2]
    ]);

    assert_eq!(result, shared::traverse(&graph, 0, 2));
}

#[test]
fn test_traverse_d12() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![0, 1]
    ]);

    assert_eq!(result, shared::traverse(&graph, 0, 1));
}

#[test]
fn test_traverse_d212() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![1, 0, 3, 6, 11]
    ]);

    assert_eq!(result, shared::traverse(&graph, 1, 11));
}

#[test]
fn test_traverse_d211() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![1, 0, 3, 6, 10]
    ]);

    assert_eq!(result, shared::traverse(&graph, 1, 10));
}

#[test]
fn test_traverse_d210() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![1, 4, 9]
    ]);

    assert_eq!(result, shared::traverse(&graph, 1, 9));
}

#[test]
fn test_traverse_d29() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![1, 4, 8]
    ]);

    assert_eq!(result, shared::traverse(&graph, 1, 8));
}

#[test]
fn test_traverse_d28() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![1, 0, 3, 7]
    ]);

    assert_eq!(result, shared::traverse(&graph, 1, 7));
}

#[test]
fn test_traverse_d27() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![1, 0, 3, 6]
    ]);

    assert_eq!(result, shared::traverse(&graph, 1, 6));
}

#[test]
fn test_traverse_d26() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![1, 5]
    ]);

    assert_eq!(result, shared::traverse(&graph, 1, 5));
}

#[test]
fn test_traverse_d25() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![1, 4]
    ]);

    assert_eq!(result, shared::traverse(&graph, 1, 4));
}

#[test]
fn test_traverse_d24() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![1, 0, 3]
    ]);

    assert_eq!(result, shared::traverse(&graph, 1, 3));
}

#[test]
fn test_traverse_d23() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![1, 0, 2]
    ]);

    assert_eq!(result, shared::traverse(&graph, 1, 2));
}

#[test]
fn test_traverse_d21() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![1, 0]
    ]);

    assert_eq!(result, shared::traverse(&graph, 1, 0));
}

#[test]
fn test_traverse_d312() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![2, 0, 3, 6, 11]
    ]);

    assert_eq!(result, shared::traverse(&graph, 2, 11));
}

#[test]
fn test_traverse_d311() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![2, 0, 3, 6, 10]
    ]);

    assert_eq!(result, shared::traverse(&graph, 2, 10));
}

#[test]
fn test_traverse_d310() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![2, 0, 1, 4, 9]
    ]);

    assert_eq!(result, shared::traverse(&graph, 2, 9));
}

#[test]
fn test_traverse_d39() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![2, 0, 1, 4, 8]
    ]);

    assert_eq!(result, shared::traverse(&graph, 2, 8));
}

#[test]
fn test_traverse_d38() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![2, 0, 3, 7]
    ]);

    assert_eq!(result, shared::traverse(&graph, 2, 7));
}

#[test]
fn test_traverse_d37() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![2, 0, 3, 6]
    ]);

    assert_eq!(result, shared::traverse(&graph, 2, 6));
}

#[test]
fn test_traverse_d36() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![2, 0, 1, 5]
    ]);

    assert_eq!(result, shared::traverse(&graph, 2, 5));
}

#[test]
fn test_traverse_d35() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![2, 0, 1, 4]
    ]);

    assert_eq!(result, shared::traverse(&graph, 2, 4));
}

#[test]
fn test_traverse_d34() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![2, 0, 3]
    ]);

    assert_eq!(result, shared::traverse(&graph, 2, 3));
}

#[test]
fn test_traverse_d32() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![2, 0, 1]
    ]);

    assert_eq!(result, shared::traverse(&graph, 2, 1));
}

#[test]
fn test_traverse_d31() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![2, 0]
    ]);

    assert_eq!(result, shared::traverse(&graph, 2, 0));
}

#[test]
fn test_traverse_d412() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![3, 6, 11]
    ]);

    assert_eq!(result, shared::traverse(&graph, 3, 11));
}

#[test]
fn test_traverse_d411() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![3, 6, 10]
    ]);

    assert_eq!(result, shared::traverse(&graph, 3, 10));
}

#[test]
fn test_traverse_d410() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![3, 0, 1, 4, 9]
    ]);

    assert_eq!(result, shared::traverse(&graph, 3, 9));
}

#[test]
fn test_traverse_d49() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![3, 0, 1, 4, 8]
    ]);

    assert_eq!(result, shared::traverse(&graph, 3, 8));
}

#[test]
fn test_traverse_d48() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![3, 7]
    ]);

    assert_eq!(result, shared::traverse(&graph, 3, 7));
}

#[test]
fn test_traverse_d47() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![3, 6]
    ]);

    assert_eq!(result, shared::traverse(&graph, 3, 6));
}

#[test]
fn test_traverse_d46() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![3, 0, 1, 5]
    ]);

    assert_eq!(result, shared::traverse(&graph, 3, 5));
}

#[test]
fn test_traverse_d45() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![3, 0, 1, 4]
    ]);

    assert_eq!(result, shared::traverse(&graph, 3, 4));
}

#[test]
fn test_traverse_d43() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![3, 0, 2]
    ]);

    assert_eq!(result, shared::traverse(&graph, 3, 2));
}

#[test]
fn test_traverse_d42() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![3, 0, 1]
    ]);

    assert_eq!(result, shared::traverse(&graph, 3, 1));
}

#[test]
fn test_traverse_d41() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![3, 0]
    ]);

    assert_eq!(result, shared::traverse(&graph, 3, 0));
}

#[test]
fn test_traverse_d512() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![4, 1, 0, 3, 6, 11]
    ]);

    assert_eq!(result, shared::traverse(&graph, 4, 11));
}

#[test]
fn test_traverse_d511() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![4, 1, 0, 3, 6, 10]
    ]);

    assert_eq!(result, shared::traverse(&graph, 4, 10));
}

#[test]
fn test_traverse_d510() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![4, 9]
    ]);

    assert_eq!(result, shared::traverse(&graph, 4, 9));
}

#[test]
fn test_traverse_d59() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![4, 8]
    ]);

    assert_eq!(result, shared::traverse(&graph, 4, 8));
}

#[test]
fn test_traverse_d58() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![4, 1, 0, 3, 7]
    ]);

    assert_eq!(result, shared::traverse(&graph, 4, 7));
}

#[test]
fn test_traverse_d57() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![4, 1, 0, 3, 6]
    ]);

    assert_eq!(result, shared::traverse(&graph, 4, 6));
}

#[test]
fn test_traverse_d56() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![4, 1, 5]
    ]);

    assert_eq!(result, shared::traverse(&graph, 4, 5));
}

#[test]
fn test_traverse_d54() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![4, 1, 0, 3]
    ]);

    assert_eq!(result, shared::traverse(&graph, 4, 3));
}

#[test]
fn test_traverse_d53() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![4, 1, 0, 2]
    ]);

    assert_eq!(result, shared::traverse(&graph, 4, 2));
}

#[test]
fn test_traverse_d52() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![4, 1]
    ]);

    assert_eq!(result, shared::traverse(&graph, 4, 1));
}

#[test]
fn test_traverse_d51() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![4, 1, 0]
    ]);

    assert_eq!(result, shared::traverse(&graph, 4, 0));
}

#[test]
fn test_traverse_d612() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![5, 1, 0, 3, 6, 11]
    ]);

    assert_eq!(result, shared::traverse(&graph, 5, 11));
}

#[test]
fn test_traverse_d611() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![5, 1, 0, 3, 6, 10]
    ]);

    assert_eq!(result, shared::traverse(&graph, 5, 10));
}

#[test]
fn test_traverse_d610() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![5, 1, 4, 9]
    ]);

    assert_eq!(result, shared::traverse(&graph, 5, 9));
}

#[test]
fn test_traverse_d69() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![5, 1, 4, 8]
    ]);

    assert_eq!(result, shared::traverse(&graph, 5, 8));
}

#[test]
fn test_traverse_d68() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![5, 1, 0, 3, 7]
    ]);

    assert_eq!(result, shared::traverse(&graph, 5, 7));
}

#[test]
fn test_traverse_d67() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![5, 1, 0, 3, 6]
    ]);

    assert_eq!(result, shared::traverse(&graph, 5, 6));
}

#[test]
fn test_traverse_d65() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![5, 1, 4]
    ]);

    assert_eq!(result, shared::traverse(&graph, 5, 4));
}

#[test]
fn test_traverse_d64() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![5, 1, 0, 3]
    ]);

    assert_eq!(result, shared::traverse(&graph, 5, 3));
}

#[test]
fn test_traverse_d63() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![5, 1, 0, 2]
    ]);

    assert_eq!(result, shared::traverse(&graph, 5, 2));
}

#[test]
fn test_traverse_d62() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![5, 1]
    ]);

    assert_eq!(result, shared::traverse(&graph, 5, 1));
}

#[test]
fn test_traverse_d61() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![5, 1, 0]
    ]);

    assert_eq!(result, shared::traverse(&graph, 5, 0));
}

#[test]
fn test_traverse_d712() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![6, 11]
    ]);

    assert_eq!(result, shared::traverse(&graph, 6, 11));
}

#[test]
fn test_traverse_d711() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![6, 10]
    ]);

    assert_eq!(result, shared::traverse(&graph, 6, 10));
}

#[test]
fn test_traverse_d710() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![6, 3, 0, 1, 4, 9]
    ]);

    assert_eq!(result, shared::traverse(&graph, 6, 9));
}

#[test]
fn test_traverse_d79() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![6, 3, 0, 1, 4, 8]
    ]);

    assert_eq!(result, shared::traverse(&graph, 6, 8));
}

#[test]
fn test_traverse_d78() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![6, 3, 7]
    ]);

    assert_eq!(result, shared::traverse(&graph, 6, 7));
}

#[test]
fn test_traverse_d76() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![6, 3, 0, 1, 5]
    ]);

    assert_eq!(result, shared::traverse(&graph, 6, 5));
}

#[test]
fn test_traverse_d75() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![6, 3, 0, 1, 4]
    ]);

    assert_eq!(result, shared::traverse(&graph, 6, 4));
}

#[test]
fn test_traverse_d74() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![6, 3]
    ]);

    assert_eq!(result, shared::traverse(&graph, 6, 3));
}

#[test]
fn test_traverse_d73() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![6, 3, 0, 2]
    ]);

    assert_eq!(result, shared::traverse(&graph, 6, 2));
}

#[test]
fn test_traverse_d72() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![6, 3, 0, 1]
    ]);

    assert_eq!(result, shared::traverse(&graph, 6, 1));
}

#[test]
fn test_traverse_d71() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![6, 3, 0]
    ]);

    assert_eq!(result, shared::traverse(&graph, 6, 0));
}

#[test]
fn test_traverse_d812() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![7, 3, 6, 11]
    ]);

    assert_eq!(result, shared::traverse(&graph, 7, 11));
}

#[test]
fn test_traverse_d811() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![7, 3, 6, 10]
    ]);

    assert_eq!(result, shared::traverse(&graph, 7, 10));
}

#[test]
fn test_traverse_d810() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![7, 3, 0, 1, 4, 9]
    ]);

    assert_eq!(result, shared::traverse(&graph, 7, 9));
}

#[test]
fn test_traverse_d89() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![7, 3, 0, 1, 4, 8]
    ]);

    assert_eq!(result, shared::traverse(&graph, 7, 8));
}

#[test]
fn test_traverse_d87() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![7, 3, 6]
    ]);

    assert_eq!(result, shared::traverse(&graph, 7, 6));
}

#[test]
fn test_traverse_d86() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![7, 3, 0, 1, 5]
    ]);

    assert_eq!(result, shared::traverse(&graph, 7, 5));
}

#[test]
fn test_traverse_d85() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![7, 3, 0, 1, 4]
    ]);

    assert_eq!(result, shared::traverse(&graph, 7, 4));
}

#[test]
fn test_traverse_d84() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![7, 3]
    ]);

    assert_eq!(result, shared::traverse(&graph, 7, 3));
}

#[test]
fn test_traverse_d83() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![7, 3, 0, 2]
    ]);

    assert_eq!(result, shared::traverse(&graph, 7, 2));
}

#[test]
fn test_traverse_d82() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![7, 3, 0, 1]
    ]);

    assert_eq!(result, shared::traverse(&graph, 7, 1));
}

#[test]
fn test_traverse_d81() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![7, 3, 0]
    ]);

    assert_eq!(result, shared::traverse(&graph, 7, 0));
}

#[test]
fn test_traverse_d912() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![8, 4, 1, 0, 3, 6, 11]
    ]);

    assert_eq!(result, shared::traverse(&graph, 8, 11));
}

#[test]
fn test_traverse_d911() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![8, 4, 1, 0, 3, 6, 10]
    ]);

    assert_eq!(result, shared::traverse(&graph, 8, 10));
}

#[test]
fn test_traverse_d910() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![8, 4, 9]
    ]);

    assert_eq!(result, shared::traverse(&graph, 8, 9));
}

#[test]
fn test_traverse_d98() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![8, 4, 1, 0, 3, 7]
    ]);

    assert_eq!(result, shared::traverse(&graph, 8, 7));
}

#[test]
fn test_traverse_d97() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![8, 4, 1, 0, 3, 6]
    ]);

    assert_eq!(result, shared::traverse(&graph, 8, 6));
}

#[test]
fn test_traverse_d96() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![8, 4, 1, 5]
    ]);

    assert_eq!(result, shared::traverse(&graph, 8, 5));
}

#[test]
fn test_traverse_d95() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![8, 4]
    ]);

    assert_eq!(result, shared::traverse(&graph, 8, 4));
}

#[test]
fn test_traverse_d94() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![8, 4, 1, 0, 3]
    ]);

    assert_eq!(result, shared::traverse(&graph, 8, 3));
}

#[test]
fn test_traverse_d93() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![8, 4, 1, 0, 2]
    ]);

    assert_eq!(result, shared::traverse(&graph, 8, 2));
}

#[test]
fn test_traverse_d92() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![8, 4, 1]
    ]);

    assert_eq!(result, shared::traverse(&graph, 8, 1));
}

#[test]
fn test_traverse_d91() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![8, 4, 1, 0]
    ]);

    assert_eq!(result, shared::traverse(&graph, 8, 0));
}

#[test]
fn test_traverse_d1012() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![9, 4, 1, 0, 3, 6, 11]
    ]);

    assert_eq!(result, shared::traverse(&graph, 9, 11));
}

#[test]
fn test_traverse_d1011() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![9, 4, 1, 0, 3, 6, 10]
    ]);

    assert_eq!(result, shared::traverse(&graph, 9, 10));
}

#[test]
fn test_traverse_d109() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![9, 4, 8]
    ]);

    assert_eq!(result, shared::traverse(&graph, 9, 8));
}

#[test]
fn test_traverse_d108() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![9, 4, 1, 0, 3, 7]
    ]);

    assert_eq!(result, shared::traverse(&graph, 9, 7));
}

#[test]
fn test_traverse_d107() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![9, 4, 1, 0, 3, 6]
    ]);

    assert_eq!(result, shared::traverse(&graph, 9, 6));
}

#[test]
fn test_traverse_d106() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![9, 4, 1, 5]
    ]);

    assert_eq!(result, shared::traverse(&graph, 9, 5));
}

#[test]
fn test_traverse_d105() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![9, 4]
    ]);

    assert_eq!(result, shared::traverse(&graph, 9, 4));
}

#[test]
fn test_traverse_d104() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![9, 4, 1, 0, 3]
    ]);

    assert_eq!(result, shared::traverse(&graph, 9, 3));
}

#[test]
fn test_traverse_d103() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![9, 4, 1, 0, 2]
    ]);

    assert_eq!(result, shared::traverse(&graph, 9, 2));
}

#[test]
fn test_traverse_d102() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![9, 4, 1]
    ]);

    assert_eq!(result, shared::traverse(&graph, 9, 1));
}

#[test]
fn test_traverse_d101() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![9, 4, 1, 0]
    ]);

    assert_eq!(result, shared::traverse(&graph, 9, 0));
}

#[test]
fn test_traverse_d11012() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![10, 6, 11]
    ]);

    assert_eq!(result, shared::traverse(&graph, 10, 11));
}

#[test]
fn test_traverse_d11010() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![10, 6, 3, 0, 1, 4, 9]
    ]);

    assert_eq!(result, shared::traverse(&graph, 10, 9));
}

#[test]
fn test_traverse_d1109() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![10, 6, 3, 0, 1, 4, 8]
    ]);

    assert_eq!(result, shared::traverse(&graph, 10, 8));
}

#[test]
fn test_traverse_d1108() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![10, 6, 3, 7]
    ]);

    assert_eq!(result, shared::traverse(&graph, 10, 7));
}

#[test]
fn test_traverse_d1107() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![10, 6]
    ]);

    assert_eq!(result, shared::traverse(&graph, 10, 6));
}

#[test]
fn test_traverse_d1106() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![10, 6, 3, 0, 1, 5]
    ]);

    assert_eq!(result, shared::traverse(&graph, 10, 5));
}

#[test]
fn test_traverse_d1105() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![10, 6, 3, 0, 1, 4]
    ]);

    assert_eq!(result, shared::traverse(&graph, 10, 4));
}

#[test]
fn test_traverse_d1104() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![10, 6, 3]
    ]);

    assert_eq!(result, shared::traverse(&graph, 10, 3));
}

#[test]
fn test_traverse_d1103() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![10, 6, 3, 0, 2]
    ]);

    assert_eq!(result, shared::traverse(&graph, 10, 2));
}

#[test]
fn test_traverse_d1102() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![10, 6, 3, 0, 1]
    ]);

    assert_eq!(result, shared::traverse(&graph, 10, 1));
}

#[test]
fn test_traverse_d1101() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![10, 6, 3, 0]
    ]);

    assert_eq!(result, shared::traverse(&graph, 10, 0));
}

#[test]
fn test_traverse_d12011() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![11, 6, 10]
    ]);

    assert_eq!(result, shared::traverse(&graph, 11, 10));
}

#[test]
fn test_traverse_d12010() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![11, 6, 3, 0, 1, 4, 9]
    ]);

    assert_eq!(result, shared::traverse(&graph, 11, 9));
}

#[test]
fn test_traverse_d1209() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![11, 6, 3, 0, 1, 4, 8]
    ]);

    assert_eq!(result, shared::traverse(&graph, 11, 8));
}

#[test]
fn test_traverse_d1208() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![11, 6, 3, 7]
    ]);

    assert_eq!(result, shared::traverse(&graph, 11, 7));
}

#[test]
fn test_traverse_d1207() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![11, 6]
    ]);

    assert_eq!(result, shared::traverse(&graph, 11, 6));
}

#[test]
fn test_traverse_d1206() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![11, 6, 3, 0, 1, 5]
    ]);

    assert_eq!(result, shared::traverse(&graph, 11, 5));
}

#[test]
fn test_traverse_d1205() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![11, 6, 3, 0, 1, 4]
    ]);

    assert_eq!(result, shared::traverse(&graph, 11, 4));
}

#[test]
fn test_traverse_d1204() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![11, 6, 3]
    ]);

    assert_eq!(result, shared::traverse(&graph, 11, 3));
}

#[test]
fn test_traverse_d1203() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![11, 6, 3, 0, 2]
    ]);

    assert_eq!(result, shared::traverse(&graph, 11, 2));
}

#[test]
fn test_traverse_d1202() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![11, 6, 3, 0, 1]
    ]);

    assert_eq!(result, shared::traverse(&graph, 11, 1));
}

#[test]
fn test_traverse_d1201() {
    let graph: VecDeque<Vec<usize>> = setup_graph_d();
    let result: VecDeque<Vec<usize>> = VecDeque::from_iter([
        vec![11, 6, 3, 0]
    ]);

    assert_eq!(result, shared::traverse(&graph, 11, 0));
}

