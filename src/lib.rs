// SPDX-License-Identifier: BSD-3-Clause
// SPDX-FileCopyrightText: 2022 Ronan Harris <miteiruka@karakunai.com>
// SPDX-FileType: SOURCE

pub mod shared;
