// SPDX-License-Identifier: BSD-3-Clause
// SPDX-FileCopyrightText: 2022 Ronan Harris <miteiruka@karakunai.com>
// SPDX-FileType: SOURCE

use bfbfs_rust::shared;

fn main() {
    shared::dummy_method("Lorem ipsum dolor sit amet.");
}
