// SPDX-License-Identifier: BSD-3-Clause
// SPDX-FileCopyrightText: 2022 Ronan Harris <miteiruka@karakunai.com>
// SPDX-FileType: SOURCE

use std::collections::VecDeque;

pub fn dummy_method(dummy: &str) -> &str {
    dummy
}

/// # Description
/// Takes a reference of an array as a target to compare with another array
/// reference to only return N amount of elements of the array in comparison
/// that does not exists in the target array by iterating on each target
/// array that contains the indexes of the other array elements to check
/// against. The max index of an element in the target array must not be bigger
/// than the length of the array in comparison;
///
/// # Examples
/// ```
/// use std::collections::VecDeque;
/// use bfbfs_rust::shared::trim_elements;
///
/// let x = trim_elements(&vec![0, 1, 4], &vec![1, 1, 1, 0, 1]);
/// let y = trim_elements(&vec![0, 1, 2], &vec![1, 1, 1, 0, 1]);
///
/// assert_eq!(vec![2], x);
/// assert_eq!(vec![4], y);
/// ```
pub fn trim_elements(target: &Vec<usize>, compare: &Vec<usize>) -> Vec<usize> {
    let mut compare_copy: Vec<usize> = compare.clone();

    for item in target {
        compare_copy[*item] = 0;
    }

    return compare_copy.into_iter().enumerate().filter(|&(_, y)| y > 0).map(|(x, _)| x).collect();
}

/// # Description
/// Takes a reference of a 2D array represeting a graph and a reference of an
/// array of routes to branch out on every matching routes ended with `current`
/// to be split away from routes without from the branching process. This 
/// function is very experimental and is the main differrence between 'BFBFS'
/// to 'PPE-BRC' and 'TREATS' that features a split backtrack branch module
/// separate from the traversing process. `branch_orchestrator` works in
/// conjunction with `trim_elements` by applying `trim_elements` on `current`
/// to avoid duplicates added to a certain route and then applying `trim_elements`
/// once more on every elements adjacent to `current` to avoid a term known as
/// circular reference or closed-loop. The only exception made to alter the
/// later behavior is when this specific adjacent `current` element equals to
/// `dest` that represents the index of the destination point.
///
/// # Examples
/// ```
/// use std::collections::VecDeque;
/// use bfbfs_rust::shared::branch_orchestrator;
///
/// let x = branch_orchestrator(&VecDeque::from_iter([vec![0, 1, 1], vec![1, 0, 1], vec![1, 1, 0]]), &VecDeque::from_iter([vec![0]]), &0, &1);
/// let y = branch_orchestrator(&VecDeque::from_iter([vec![0, 1, 1], vec![1, 0, 1], vec![1, 1, 0]]), &VecDeque::from_iter([vec![1]]), &1, &2);
///
/// assert_eq!(VecDeque::from_iter([vec![0, 1], vec![0, 2]]), x);
/// assert_eq!(VecDeque::from_iter([vec![1, 0], vec![1, 2]]), y);
/// ```
pub fn branch_orchestrator(graph: &VecDeque<Vec<usize>>,routes: &VecDeque<Vec<usize>>, current: &usize, dest: &usize) -> VecDeque<Vec<usize>> {
    let mut routes_to_branch: VecDeque<Vec<usize>> = VecDeque::new();
    let mut result: VecDeque<Vec<usize>> = VecDeque::new();

    for route in routes {
        if route.last().unwrap() == current && route.last().unwrap() != dest {
            routes_to_branch.push_back(route.clone());
        } else {
            result.push_back(route.clone());
        }
    }

    while &routes_to_branch.len() > &0 {
        let first_item: Vec<usize> = routes_to_branch.pop_front().unwrap();
        let exclusive_adjacency = trim_elements(&first_item, &graph[*current]);
       
        for item in exclusive_adjacency {
            if trim_elements(&first_item, &graph[item]).len() > 0 || &item == dest {
                result.push_back([first_item.clone(), vec![item]].concat());
            }
        }
    }
    
    return result;
}

/// # Description
/// Takes a reference of a 2D array represeting a graph and a reference of an
/// array of routes to check for any incomplete route to either complete or
/// to be removed if `branch_orchestrator` decide that specific route has no
/// more left to complete with. Incomplete would refer to routes that does not
/// ends with `dest` as its last element.
///
/// # Examples
/// ```
/// use std::collections::VecDeque;
/// use bfbfs_rust::shared::route_check;
///
/// let x = route_check(&VecDeque::from_iter([vec![0, 1, 1], vec![1, 0, 1], vec![1, 1, 0]]), &VecDeque::from_iter([vec![0, 1], vec![0, 2]]), &1);
/// let y = route_check(&VecDeque::from_iter([vec![0, 1, 1], vec![1, 0, 1], vec![1, 1, 0]]), &VecDeque::from_iter([vec![1, 0], vec![1, 2]]), &2);
///
/// assert_eq!(VecDeque::from_iter([vec![0, 1], vec![0, 2, 1]]), x);
/// assert_eq!(VecDeque::from_iter([vec![1, 2], vec![1, 0, 2]]), y);
/// ```
pub fn route_check(graph: &VecDeque<Vec<usize>>, routes: &VecDeque<Vec<usize>>, dest: &usize) -> VecDeque<Vec<usize>> {
    let mut result: VecDeque<Vec<usize>> = routes.clone();
    let mut removed_indexes: Vec<usize> = Vec::new();

    for index in 0..routes.len() {
        if routes[index].last().unwrap() != dest {
            let branch_result: VecDeque<Vec<usize>> = branch_orchestrator(&graph, &VecDeque::from_iter([routes[index].clone()]), routes[index].last().unwrap(), &dest);

            removed_indexes.push(index);

            if branch_result.len() > 0 {
                for item in branch_result {
                    if item.last().unwrap() == dest {
                        result.push_back(item);
                    }
                }
            }
        }
    }

    for index in (0..removed_indexes.len()).rev() {
        result.remove(removed_indexes[index]);
    }

    return result;
}

/// # Description
/// Takes a reference of a 2D array represeting a graph and two `usize` as
/// a parameter for both `start` and `dest` to be used as a reference for this
/// function to begin and end its iteration. The final output is an array of
/// routes from the least amount of hops all the way to the most. To avoid any
/// broken routes being returned, `traverse` will check the array of routes
/// first with `route_check` as a last resort. In simple terms, `traverse` is
/// a BFS algorithm that calls `branch_orchestrator` for every element on the
/// queue to build an array of routes.
///
/// # Examples
/// ```
/// use std::collections::VecDeque;
/// use bfbfs_rust::shared::traverse;
///
/// let x = traverse(&VecDeque::from_iter([vec![0, 1, 1], vec![1, 0, 1], vec![1, 1, 0]]), 0, 1);
/// let y = traverse(&VecDeque::from_iter([vec![0, 1, 1], vec![1, 0, 1], vec![1, 1, 0]]), 1, 2);
///
/// assert_eq!(VecDeque::from_iter([vec![0, 1], vec![0, 2, 1]]), x);
/// assert_eq!(VecDeque::from_iter([vec![1, 2], vec![1, 0, 2]]), y);
/// ```
pub fn traverse(graph: &VecDeque<Vec<usize>>, start: usize, dest: usize) -> VecDeque<Vec<usize>>{
    let mut visited: Vec<usize> = vec![usize::MAX; graph.len()];
    let mut routes: VecDeque<Vec<usize>> = VecDeque::from([vec![start]]);
    let mut queue: VecDeque<usize> = VecDeque::from([start]);
    let indexed_graph: VecDeque<Vec<usize>> = graph.clone().into_iter().map(|e| e.into_iter().enumerate().filter(|(_, y)| y > &0).map(|(x, _)| x).collect()).collect();

    while &queue.len() > &0 {
        let first_item: usize = queue.pop_front().unwrap();

        if visited[first_item] != first_item {
            queue.append(&mut VecDeque::from_iter(indexed_graph[first_item].clone()));
        }
        
        routes = branch_orchestrator(&graph, &routes, &first_item, &dest);
        visited[first_item] = first_item;
    }

    return route_check(&graph, &routes, &dest);
}

/// # Description
/// Takes an array of routes to print into standard output.
///
/// # Examples
/// ```
/// use std::collections::VecDeque;
/// use bfbfs_rust::shared::route_print;
///
/// route_print(&VecDeque::from_iter([vec![0, 1], vec![0, 2, 1]]), 0, 1);
/// route_print(&VecDeque::from_iter([vec![1, 2], vec![1, 0, 2]]), 1, 2);
/// ```
pub fn route_print(graph: &VecDeque<Vec<usize>>, start: usize, dest: usize) {
    let mut string_out: String = String::new();
    let mut row_counter: usize = 0;

    for row in 0..graph.len() {
        if graph[row].len() > 0 {
            row_counter += 1;
            string_out += &format!("{} -> ", row_counter);
            
            for cell in &graph[row] {
                if cell >= &0 {
                    string_out += &format!("{} ", cell + 1);
                }
            }
            
            string_out += &format!("\n");
        }
    }

    string_out += &format!("\nFROM: {} | TO: {} | TOTAL: {}", start + 1, dest + 1, row_counter);

    println!("{}", string_out);
}
