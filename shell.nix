{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {

  buildInputs = with pkgs; [
    # Rust
    cargo
    rustc
    rls
    rust-analyzer
  ];

  shellHook = ''
    export PATH="${pkgs.rustc}/bin/rustc:$PATH"
    export RUST_BACKTRACE=1
    clear
  '';
}
